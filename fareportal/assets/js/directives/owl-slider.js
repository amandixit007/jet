angular.module('jetSmart')
.directive("owlCarousel", function() {
	return {
		restrict: 'C',
		transclude: false,
		link: function (scope) {
			scope.initCarousel = function(element) {
			  // provide any default options you want
				var defaultOptions = {
					margin: 0,
					nav: false,
					center:false,
					mouseDrag: true,
					autoplay: true,	
					loop: true,
					responsive: {
					  0: {
						items: 1
					  },
					  600: {
						items: 1
					  },
					  1000: {
						items: 1
					  }
					}
				};
				var customOptions = scope.$eval($(element).attr('data-options'));
				// combine the two options objects
				for(var key in customOptions) {
					defaultOptions[key] = customOptions[key];
				}
				// init carousel
				$(element).owlCarousel(defaultOptions);
			};
		}
	};
})
.directive('owlCarouselItem', [function() {
	return {
		restrict: 'A',
		transclude: false,
		link: function(scope, element) {
		  // wait for the last item in the ng-repeat then call init
			if(scope.$last) {
				scope.initCarousel(element.parent());
			}
		}
	};
}])