angular.module('jetSmart')
.directive('bookingForm', function(){
	return {
		restrict : 'A',
		link : function(scope, elemt, attr){
			elemt.click(function(e){
				e.stopPropagation()
			})
			elemt.on('click', 'a.booking-tgl, .modify-btn a.btn', function(e){
				$('.booking-module-wrapper').addClass('active-booking');
				e.stopPropagation();
			});
			elemt.on('click', '.booking-area, .testdiv', function(e){
				e.stopPropagation();
			});
			elemt.on('click', 'a.close-booking-tgl, body', function(e){
				$('.booking-module-wrapper').removeClass('active-booking');
			})
			//
			elemt.find('.return-date-wrapper').slideUp();
			elemt.on('click', 'input#roundTrip + label', function(){
				$('.return-date-wrapper').slideDown();
				$('.multicity-box').slideUp();
			})
			elemt.find('.multicity-box').slideUp();
			elemt.on('click', 'input#oneWay + label', function(){
				$('.return-date-wrapper').slideUp();
				$('.multicity-box').slideUp();
			})
			elemt.on('click', 'input#multicity + label', function(){
				$('.multicity-box').slideDown();
				$('.return-date-wrapper').slideUp();
			})
		},
	}
})
.directive('modifySearch', function(){
	return {
		restrict : 'A',
		link : function(scope, elemt, attr){
			console.log('modifySearch')
			//
			setTimeout(function(){
				if(elemt.find('input#roundTrip').is(':checked')){
					elemt.find('.return-date-wrapper').slideDown();
				}else{
					elemt.find('.return-date-wrapper').slideUp();
				}
			},500)
			elemt.find('input#roundTrip + label').click(function(){
				$('.return-date-wrapper').slideDown();
				$('.multicity-box').slideUp();
			})
			elemt.find('input#oneWay + label').click(function(){
				$('.return-date-wrapper').slideUp();
				$('.multicity-box').slideUp();
			})
			setTimeout(function(){
				if(elemt.find('input#multicity').is(':checked')){
					elemt.find('.multicity-box').slideDown();
				}else{
					elemt.find('.multicity-box').slideUp();
				}
			},500)
			elemt.find('input#multicity + label').click(function(){
				$('.multicity-box').slideDown();
				$('.return-date-wrapper').slideUp();
			})
			//
			// elemt.find('.status .switch').click(function(){
			// 	if ($('.switch input').is(':checked')) {
			// 		// $('.advanced-booking-engine').slideDown();
			// 	}
			// 	else{
			// 		$('.advanced-booking-engine').slideUp();
			// 	}
			// })
		},
	}
})
.directive('moreInfo', function(){
	return {
		restrict : 'A',
		link : function(scope, elemt, attr){
			elemt.on('click', '.more-info-tgl', function(){
				$(this).parents('.available-flights-wrapper').toggleClass('active-details');
				$(this).parents('.available-flights-wrapper').children().children('.flight-fare').slideToggle();
			});
			elemt.on('click', 'p.close-flights-details', function(){
				$(this).parents('.available-flights-wrapper').removeClass('active-details');
				$(this).parents('.available-flights-wrapper').children().children('.flight-fare').slideDown();
			});
		}
	}
})
.directive('btDate', function(){
	return {
		restrict : 'A',
		link : function(scope, elemt, attr){
			elemt.bootstrapMaterialDatePicker({
				format: 'DD MMMM YYYY',
				time: false,
				clearButton: true,
				minDate : new Date(),
			});
		}
	}
})
.directive('btTime', function(){
	return {
		restrict : 'A',
		link : function(scope, elemt, attr){
			elemt.bootstrapMaterialDatePicker({
				date: false,
				shortTime: true,
				format: 'HH:mm'
			});
		}
	}
})
.directive('flightInfoToggle', function(){
	return {
		restrict : 'A',
		link : function(scope, elemt, attr){
			elemt.find('.view-info-tgl').click(function(){
				$(this).next('div').slideToggle();
				$(this).parent('.info-button-wrapper').parent('.flights-details').parents('.available-flights-wrapper').siblings().find('.view-info-tgl').next('div').slideUp();
				$(this).parent('.info-button-wrapper').parent('.flights-details').parents('.available-flights-wrapper').toggleClass('active-flights-wrapper');
				$(this).parent('.info-button-wrapper').parent('.flights-details').parents('.available-flights-wrapper').siblings().removeClass('active-flights-wrapper');
				$(this).parent('.info-button-wrapper').parent('.flights-details').parents('.available-flights-wrapper').find('.flight-fare').slideDown();
				$(this).parent('.info-button-wrapper').parent('.flights-details').parents('.available-flights-wrapper').siblings().find('.flight-fare').slideDown();
				$(this).parent('.info-button-wrapper').parent('.flights-details').parents('.available-flights-wrapper').removeClass('active-details');
				$(this).parent('.info-button-wrapper').parent('.flights-details').parents('.available-flights-wrapper').siblings().removeClass('active-details');
			});		
		}
	}
})
.directive('datePicker', ['$window', function($window) {

    return {
      scope: {
        onChange: '&',
        date: '=',
      },
      link: function($scope, $element, $attrs) {

        var format  = 'DD MMMM YYYY';
        var time = $attrs.time;
        console.log(time)
        var options = {
          format: format,
          time: false,
        };

        if ($attrs.minDate) {
        	console.log($attrs.minDate)
          if ($attrs.minDate === 'today') {
            options.minDate = $window.moment();
          } else {
            options.minDate = $attrs.minDate;
          }
        }

        if ($scope.date) {
          options.currentDate = $scope.date;
        }

        $($element).bootstrapMaterialDatePicker(options);
        $element.on('change', function(event, date) {
        	// console.log(date)
          $scope.$apply(function() {
            $scope.date = date.format(format);
            $scope.onChange({
              date: date
            });
          });
        });

        $scope.$watch('date', function(newValue, oldValue) {
        	// console.log(newValue, oldValue)
          if (newValue !== oldValue) {
            $($element).bootstrapMaterialDatePicker('setDate', newValue);
          }
        });
      }
    };

  }]);