(function(){
	var dependency = ['ui.router','ngStorage', 'angucomplete-alt', 'ui.bootstrap']
	
	angular.module('jetSmart', dependency)
	.config(['$stateProvider','$urlRouterProvider', '$locationProvider', function($stateProvider, $urlRouterProvider, $locationProvider) {
		console.log('app initiate')
		baseUrl = 'http://localhost:8080'
		$locationProvider.hashPrefix('');
		$urlRouterProvider.otherwise('/');
		/**/
		$stateProvider
		.state('home', {
			url: '/',
			views : {
				'mainView':{
					templateUrl: 'assets/views/home.html',
					controller: 'homeCtrl'
				},
			}
		})
		.state('searchFlight', {
			url: '/search',
			views : {
				'mainView':{
					templateUrl: 'assets/views/search.html',
					controller: 'searchCtrl'
				},
			}
		})
		.state('flightDetails', {
			url: '/inquiry',
			views : {
				'mainView':{
					templateUrl: 'assets/views/inquery.html',
					controller: 'flightDetailsCtrl'
				},
			}
		})
	    /**/
	}])
	.filter('startFrom', function() {
	    return function(input, start) {
	        start = +start; //parse to int
	        return input.slice(start);
	    }
	});

})()