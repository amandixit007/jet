angular.module('jetSmart')
.factory('user', ['$http', '$q', function($http, $q){
	return {

		login : function(data){

			var deff=$q.defer(data);

			var url = baseUrl+'/fareportal/user_login'
			
			$http.post(url,data).then(function(objS){
				deff.resolve(objS)
			},function(objE){
				deff.reject(objE);
			})

			return deff.promise;
		},

		signup : function(data){

			var deff=$q.defer(data);

			var url = baseUrl+'/fareportal/user_signup'
			
			$http.post(url,data).then(function(objS){
				deff.resolve(objS)
			},function(objE){
				deff.reject(objE);
			})

			return deff.promise;
		},

		inquiry : function(data){

			var deff=$q.defer(data);
			console.log(data)

			var url = baseUrl+'fareportal/user_booking_conformation?token='+localStorage.token
			
			$http.post(url,data).then(function(objS){
				deff.resolve(objS)
			},function(objE){
				deff.reject(objE);
			})

			return deff.promise;
		}
	}
}])