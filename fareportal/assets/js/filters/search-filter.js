angular.module('jetSmart')
.filter('searchFilter', function() {
	return function(input, options) {
		input = input || '';
		options = options || '';
		// console.log('input==>', input)
		// console.log('options==>', options)
		var out = []
		//
		var aircraft_type = []
		var facility = []

		angular.forEach(options.aircraft_type, function(value, key){
			// console.log('filter value==>', value)
			// console.log('filter key==>', key)
			c = key
			if(value == true){
				switch(c){
					case 'heavy_jet':
						aircraft_type.push('HEAVY JET')
						break;
					case 'super_mid_jet':
						aircraft_type.push('SUPER MID JET')
						break;
					case 'mid_jet':
						aircraft_type.push('MID JET')
						break;
					case 'light_jet':
						aircraft_type.push('LIGHT JET')
						break;
					case 'turbo_props':
						aircraft_type.push('Turbo Prop')
						break;
				}
			}
		});
		// console.log('aircraft_type==>',aircraft_type)


		angular.forEach(options.facility, function(value, key){
			// console.log('filter facility value==>', value)
			// console.log('filter facility key==>', key)
			if(value == true){
				facility.push(key)
			}
		});
		// console.log('facility==>',facility)

		//
		var aircraftTypeFilter = function(cb){
			if(aircraft_type.length > 0){
				if(aircraft_type.indexOf(input[i].documents.aircraft_type) > -1){
					cb()
				}
			}else{
				cb()
			}
		}
		//
		var facilityFilter = function(cb){
			if(facility.length > 0){
				for (var j = 0; j < facility.length; j++) {
					if(input[i].documents.facilities[facility[j]] == 'YES'){
						cb()
					}
				}
			}else{
				cb()
			}
		}
		//

		/* start filtering */
		for (var i = 0; i < input.length; i++) {
			/* price cheking */
			// if(input[i].total < options.price && typeof input[i].total === 'number'){
				/* seats checking */
				if(input[i].documents.passangers >= options.seat && typeof input[i].documents.passangers === 'number'){
					console.log()
					aircraftTypeFilter(function(){
						facilityFilter(function(){
							out.push(input[i])
						})
					})
				}
				/* seats checking end */
			// }
			/* price cheking end */
		}
		/* end filtering */
		// console.log('search filter',out)
		// console.log(out)
		return out;
	};
})