angular.module('jetSmart')
.controller('flightDetailsCtrl', ['$scope', 'user', 'custom', function($scope, user, custom){
	console.log('flightDetailsCtrl init')

	var localData = JSON.parse(localStorage.localData)
	$scope.aircraft = JSON.parse(localStorage.aircraft)

	console.log($scope.aircraft)
	// var jet = JSON.parse(localStorage.jet)
	$scope.contactDetails = {
		title:'Mr.'
	}
	$scope.passengerDetails = []
	$scope.isDetailsSubmit = false
	$scope.isAllOk = false

	for (var i = 0; i < Math.floor(localData.traveller); i++) {
		$scope.passengerDetails[i] = {
			title : 'Mr.',
			food : 'veg',
			nationlity : 'indian'
		}
	}

	console.log(localData)

	$scope.createInquiry = function(){
		console.log($scope.contactDetails.phone_number)
		console.log($scope.contactDetails.phone_number.toString())
		if($scope.contactDetails.phone_number.toString().length != 10){
			custom.alert('Phone Number Must be 10 digit','Error')
			return
		}
		if($scope.contactDetails.alternate_number.toString().length != 10){
			custom.alert('Alternate Mobile Number Must be 10 digit','Error')
			return
		}
		$scope.inqueryObj = {
			roots : localData.roots,
			pessanger_detail : $scope.passengerDetails,
			contact : $scope.contactDetails,
			aircraftId : $scope.aircraft._id
		}
		for (var i = 0; i < $scope.inqueryObj.roots.length; i++) {
			delete $scope.inqueryObj.roots[i].departureTime
		}
		$scope.isDetailsSubmit = true
		console.log($scope.inqueryObj)
	}
	$scope.submitInquiry = function(){
		user.inquiry($scope.inqueryObj)
		.then(function(objS){
			if(objS.status===200){
				$scope.isDetailsSubmit = false
				$scope.isAllOk = true
				var monthNames = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];
				var date = new Date(objS.data.created_at)
				$scope.booking = {
					number : objS.data.bookingNumber,
					time : (date.getDate()<10?'0'+date.getDate():date.getDate())+' '+monthNames[date.getMonth()]+' '+ date.getFullYear()+' '+ (date.getHours()>12?date.getHours()-12:date.getHours())+':'+ date.getMinutes()+(date.getHours()>12?'pm':'am')
					// time : (date.getDate()<10?'0'+date.getDate():date.getDate())+' '+monthNames[date.getMonth()]+' '+ date.getFullYear()+' '+ (date.getHours()>12?date.getHours()-12:date.getHours())+':'+ date.getMinutes()+(date.getHours()>12?'pm':'am')
				}
			}
			console.log(objS)
		},function(objE){
			console.log(objE)
		})
	}
}])