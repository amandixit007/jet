angular.module('jetSmart')
.controller('homeCtrl', ['$scope', 'search', '$filter', '$localStorage', '$state', 'custom', function($scope, search, $filter, $localStorage, $state, custom){
	console.log('homeCtrl atteched')
	
	$scope.airportDropdown = [];
	$scope.showAirportList = false;
	$scope.searchType = false;

	search.getAll().then(function(objS){
		console.log('all details')
		console.log(objS)
		var useableData = []
		for (var i = 0; i < objS.length; i++) {
			var allData = objS[i]
			/**/
			for (var j = 0; j < allData.airportList.length; j++) {
				var airport = allData.airportList[j]
				useableData.push({
					airport : airport.airport,
					stateName : allData.states.stateName,
					countryName : allData.countryName,
					cityName : allData.states.cities.cityName,
					iata : airport.iata,
					icao : airport.icao,
					_id : airport._id,
				})
			}
			/**/
		}

		$scope.airportDropdown = useableData
		console.log($scope.airportDropdown)
		// console.log(JSON.stringify($scope.airportDropdown))
		// console.log(JSON.stringify(objS))
	},function(objE){console.log(objE)})

	var searchDummyObj = {
		tripType : 'oneWay',
		traveller : '1',
		nationality : 'Indian',
		price : '10000000',
		searchType : 'normalSearch',
		roots : [
			{	
				originName : '',
				origin : '',//59d628eeb4dc9618b579e9bc
				destinationName : '',
				destination : '',//59d628eeb4dc9618b579e88f
				departureDate : '',
				departureTime : '',
			},
			{	
				originName : '',
				origin : '',
				destinationName : '',
				destination : '',
				departureDate : '',
				departureTime : '',
			},
		],
		aircraft_type : {
			heavy_jet : false,
			super_mid_jet : false,
			mid_size_jet : false,
			light_jet : false,
			turbo_props : false,
		},
		facility : {
			flight_attendant : false,
			lavatory : false,
			satellite_phone : false,
			wifi : false,
		},
	}

	$scope.searchObj = searchDummyObj

	$scope.resetSearchForm = function(){
		console.log(searchDummyObj)
		$scope.searchObj = {
		tripType : 'oneWay',
		traveller : '1',
		nationality : 'Indian',
		price : '10000000',
		searchType : 'normalSearch',
		roots : [
			{	
				originName : '',
				origin : '',//59d628eeb4dc9618b579e9bc
				destinationName : '',
				destination : '',//59d628eeb4dc9618b579e88f
				departureDate : '',
				departureTime : '',
			},
			{	
				originName : '',
				origin : '',
				destinationName : '',
				destination : '',
				departureDate : '',
				departureTime : '',
			},
		],
		aircraft_type : {
			heavy_jet : false,
			super_mid_jet : false,
			mid_size_jet : false,
			light_jet : false,
			turbo_props : false,
		},
		facility : {
			flight_attendant : false,
			lavatory : false,
			satellite_phone : false,
			wifi : false,
		},
	}
	}

	$scope.submitSearch = function(d){

		/**/
		for (var i = 0; i < d.roots.length; i++) {
			
			var _root = d.roots[i]

			var originArray = _root.originName.split(",");
			var destinationArray = _root.destinationName.split(",");

			for(var j = 0; j < $scope.airportDropdown.length; j++){
				if(originArray[0] == $scope.airportDropdown[j].airport){
					_root.origin = $scope.airportDropdown[j]._id
					break;
				}
			}
			for(var j = 0; j < $scope.airportDropdown.length; j++){
				if(destinationArray[0] == $scope.airportDropdown[j].airport){
					_root.destination = $scope.airportDropdown[j]._id
					break;
				}
			}
		}
		/**/
		switch (d.tripType){
			case 'oneWay':
				d.roots = [d.roots[0]];
				console.log('oneWay =>', d);
				break;
			case 'roundTrip':
				d.roots[1] = {
					origin: d.roots[0].destination,
					destination : d.roots[0].origin,
					originName: d.roots[0].destinationName,
					destinationName : d.roots[0].originName,
					departureDate : d.roots[1].departureDate,
					departureTime : d.roots[1].departureTime,
				}
				d.roots = [d.roots[0],d.roots[1]];
				console.log('roundTrip =>',d)
				break;
			case 'multicity':
				console.log('multicity =>',d)
				$localStorage.localData = d;//obj
				break;
		}
		localStorage.setItem('localData', JSON.stringify(d))
		var searchableObj = JSON.parse(localStorage.getItem('localData'))

		search.flights(searchableObj).then(function(objS){
			if(objS.message == 'No route found'){
				custom.alert('our system is unable to calculate this route, \nplease contact our customer care')
				// alert('our system is unable to calculate this route, \nplease contact our customer care')
			}else{
				$state.go('searchFlight')
			}
		})
	}

	$scope.autoFill = function(index){
		setTimeout(function(){
			if(angular.isDefined($scope.searchObj.roots[index+1]))
			$scope.searchObj.roots[index+1].originName = $scope.searchObj.roots[index].destinationName;
		}, 500)
	}

	$scope.addMulticity = function(index){
		var obj = {	
				originName : $scope.searchObj.roots[$scope.searchObj.roots.length - 1].destinationName,
				origin : $scope.searchObj.roots[$scope.searchObj.roots.length - 1].destination,
				destinationName : '',
				destination : '',
				departureDate : '',
				departureTime : '',
			};
		$scope.searchObj.roots.splice(index+1, 0, obj);
	}
	$scope.removeMulticity = function(index){
		$scope.searchObj.roots.splice(index, 1);
	}
}])
.factory("custom",['$q',function($q){
  var objCustom={}
  objCustom.alert=function(content,title){
    $.alert({
      title: title||"Error",
      content: content,
      animation: 'rotateYR',
      theme: 'supervan',
      closeAnimation: 'rotateYR',
       animationBounce: 2,
       animationSpeed:800
    });
  }

  return objCustom;
}])