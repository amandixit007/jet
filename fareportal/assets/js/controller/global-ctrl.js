angular.module('jetSmart')
.controller('globalCtrl',['$scope', '$location', '$rootScope', 'custom', 'user', '$state', function($scope, $location, $rootScope, custom, user, $state){
	$rootScope.signup = {}
	$rootScope.login = {}
	$scope.isActive = function(route) {
		return route === $location.path();
	}

	$scope.submitLogin = function(d){
		user.login(d)
		.then(function(objS){
			console.log(objS)
			if(objS.data.code == 204){
				custom.alert(objS.data.message)
				$rootScope.login.password = ''
			}
			if(angular.isDefined(objS.data.token)){
				console.log('here')
				localStorage.token = objS.data.token
				localStorage.user = JSON.stringify(objS.result)
				$state.go('flightDetails')
			}
		},function(objE){
			console.log(objE)
		})
	}
	$scope.submitSignup = function(d){
		if(d.password !== d.cPassword){
			alert('password and confirm password must be same')
			return
		}
		console.log(d)
		user.signup(d)
		.then(function(objS){
			console.log(objS)
			if(objS.status == 200){
				custom.alert('plz login','success')
			}
			$rootScope.signup={}
		},function(objE){
			console.log(objE)
		})
	}
}])