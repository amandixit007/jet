angular.module('jetSmart')
.controller('searchCtrl', ['$scope', 'search', '$filter', '$state', 'custom', '$location', 'pdf', function($scope, search, $filter, $state, custom, $location, pdf){
	console.log('searchCtrl atteched')

	// pdf.create({})

	var dropdown = [];

	var searchableObj = JSON.parse(localStorage.getItem('localData'))
	$scope.filterOptions = {
		price : parseInt(searchableObj.price),
		seat : parseInt(searchableObj.traveller),
		aircraft_type : searchableObj.aircraft_type,
		facility : searchableObj.facility,
	}

	search.getAll().then(function(objS){
		var useableData = []
		for (var i = 0; i < objS.length; i++) {
			var allData = objS[i]
			/**/
			for (var j = 0; j < allData.airportList.length; j++) {
				var airport = allData.airportList[j]
				useableData.push({
					airport : airport.airport,
					stateName : allData.states.stateName,
					countryName : allData.countryName,
					cityName : allData.states.cities.cityName,
					iata : airport.iata,
					icao : airport.icao,
					_id : airport._id,
				})
			}
			/**/
		}

		$scope.airportDropdown = useableData

		switch (searchableObj.tripType){
			case 'oneWay':
				searchableObj.roots = [searchableObj.roots[0]];
				// console.log('oneWay =>', d);
				break;
			case 'roundTrip':
				searchableObj.roots[1] = {
					origin: searchableObj.roots[0].destination,
					destination : searchableObj.roots[0].origin,
					originName: searchableObj.roots[0].destinationName,
					destinationName : searchableObj.roots[0].originName,
					departureDate : searchableObj.roots[1].departureDate,
					departureTime : searchableObj.roots[1].departureTime,
				}
				searchableObj.roots = [searchableObj.roots[0],searchableObj.roots[1]];
				// console.log('roundTrip =>',d)
				break;
			case 'multicity':
				// console.log('multicity =>',d)
				searchableObj = searchableObj;//obj
				break;
		}
		searchFlights()

	},function(objE){console.log(objE)})

	$scope.searchObj = searchableObj
	console.log('$scope.searchObj',$scope.searchObj)

	$scope.departureDate = searchableObj.roots[0].departureDate;
	$scope.departureTime = searchableObj.roots[0].departureTime;
	$scope.traveller = searchableObj.traveller;

	$scope.resetSearchForm = function(){
		$scope.searchObj = {
			tripType : 'oneWay',
			traveller : '1',
			nationality : 'Indian',
			price : '10000000',
			searchType : 'normalSearch',
			roots : [
				{	
					originName : '',
					origin : '',//59d628eeb4dc9618b579e9bc
					destinationName : '',
					destination : '',//59d628eeb4dc9618b579e88f
					departureDate : '',
					departureTime : '',
				},
				{	
					originName : '',
					origin : '',
					destinationName : '',
					destination : '',
					departureDate : '',
					departureTime : '',
				},
			],
			aircraft_type : {
				heavy_jet : false,
				super_mid_jet : false,
				mid_size_jet : false,
				light_jet : false,
				turbo_props : false,
			},
			facility : {
				flight_attendant : false,
				lavatory : false,
				satellite_phone : false,
				wifi : false,
			},
		}
	}
	//
	$scope.isActive = function(route) {
		console.log('is active')
		return route === $location.path();
	}
	/*form */
	$scope.autoFill = function(index){
		setTimeout(function(){
			if(angular.isDefined($scope.searchObj.roots[index+1]))
			$scope.searchObj.roots[index+1].originName = $scope.searchObj.roots[index].destinationName;
		}, 500)
	}

	$scope.addMulticity = function(index){
		var obj = {	
				originName : $scope.searchObj.roots[$scope.searchObj.roots.length - 1].destinationName,
				origin : $scope.searchObj.roots[$scope.searchObj.roots.length - 1].destination,
				destinationName : '',
				destination : '',
				departureDate : '',
				departureTime : '',
			};
		$scope.searchObj.roots.splice(index+1, 0, obj);
	}
	$scope.removeMulticity = function(index){
		$scope.searchObj.roots.splice(index, 1);
	}

	$scope.submitSearch = function(d){

		/**/
		for (var i = 0; i < d.roots.length; i++) {
			
			var _root = d.roots[i]

			var originArray = _root.originName.split(",");
			var destinationArray = _root.destinationName.split(",");

			for(var j = 0; j < $scope.airportDropdown.length; j++){
				if(originArray[0] == $scope.airportDropdown[j].airport){
					_root.origin = $scope.airportDropdown[j]._id
					break;
				}
			}
			for(var j = 0; j < $scope.airportDropdown.length; j++){
				if(destinationArray[0] == $scope.airportDropdown[j].airport){
					_root.destination = $scope.airportDropdown[j]._id
					break;
				}
			}
		}
		/**/
		switch (d.tripType){
			case 'oneWay':
				d.roots = [d.roots[0]];
				console.log('oneWay =>', d);
				break;
			case 'roundTrip':
				d.roots[1] = {
					origin: d.roots[0].destination,
					destination : d.roots[0].origin
				}
				d.roots = [d.roots[0],d.roots[1]];
				console.log('roundTrip =>',d)
				break;
			case 'multicity':
				console.log('multicity =>',d)
				localStorage.localData = d;//obj
				break;
		}
		localStorage.setItem('localData', JSON.stringify(d))
		var searchableObj = JSON.parse(localStorage.getItem('localData'))

		search.flights(searchableObj).then(function(objS){
			if(objS.message == 'No route found'){
				alert('our system is unable to calculate this route, \nplz contact our customer care')
			}else{
				/**/
				for (var i = 0; i < objS.length; i++) {
					if(angular.isDefined(objS[i].duration)){
						for (var j = 0; j < objS[i].duration.length; j++){
							/**/
							var origin = objS[i].duration[j].data.fromAirport
							var destination = objS[i].duration[j].data.toAirport
							objS[i].duration[j].hours=Math.floor(objS[i].duration[j].duration/60);
							objS[i].duration[j].min=Math.floor(objS[i].duration[j].duration%60)
							// var timeArray = objS[i].duration[j].duration.toString().split(".");
							// objS[i].duration[j].hours = timeArray[0];
							// objS[i].duration[j].min = (parseInt(timeArray[1])*60)/timeArray[1].length;
							// objS[i].duration[j].min = parseInt(objS[i].duration[j].min.toString().substring(0, 2))
							/**/

							objS[i].duration[j].info = {};

							for (var k = 0; k < $scope.airportDropdown.length; k++) {
								var _data = $scope.airportDropdown[k]

								//origin
								if(_data._id == origin){
									objS[i].duration[j].info.originIata = _data.iata
									objS[i].duration[j].info.originIcao = _data.icao
									objS[i].duration[j].info.originName = _data.cityName
								}
								//destination
								if(_data._id == destination){
									objS[i].duration[j].info.destinationIata = _data.iata
									objS[i].duration[j].info.destinationIcao = _data.icao
									objS[i].duration[j].info.destinationName = _data.cityName
								}
							}

						}
					}
					if(angular.isDefined(objS[i].documents.image_gallary)){
						for (var j = 0; j < objS[i].documents.image_gallary.length; j++) {
							objS[i].documents.image_gallary[j] = baseUrl + '/' + objS[i].documents.image_gallary[j]
						}
					}else{
						objS[i].documents.image_gallary = ['assets/img/default-slide.jpg']
					}
				}

				$scope.searchResult = objS

				console.log('searchResult',$scope.searchResult)
				/**/
			}
		})
	}

	var searchFlights = function(){
		console.log('Search options==>',searchableObj)
		search.flights(searchableObj).then(function(objS){
			// console.log(objS)
			for (var i = 0; i < objS.length; i++) {
				if(angular.isDefined(objS[i].duration)){
					for (var j = 0; j < objS[i].duration.length; j++){
						/**/
						var origin = objS[i].duration[j].data.fromAirport
						var destination = objS[i].duration[j].data.toAirport

						objS[i].duration[j].hours=Math.floor(objS[i].duration[j].duration/60);
						objS[i].duration[j].min=Math.floor(objS[i].duration[j].duration%60)
						// var timeArray = objS[i].duration[j].duration.toString().split(".");
						// objS[i].duration[j].hours = timeArray[0];
						// objS[i].duration[j].min = (parseInt(timeArray[1])*60)/timeArray[1].length;
						// objS[i].duration[j].min = parseInt(objS[i].duration[j].min.toString().substring(0, 2))
						/**/

						objS[i].duration[j].info = {};

						for (var k = 0; k < $scope.airportDropdown.length; k++) {
							var _data = $scope.airportDropdown[k]

							//origin
							if(_data._id == origin){
								objS[i].duration[j].info.originIata = _data.iata
								objS[i].duration[j].info.originIcao = _data.icao
								objS[i].duration[j].info.originName = _data.cityName
							}
							//destination
							if(_data._id == destination){
								objS[i].duration[j].info.destinationIata = _data.iata
								objS[i].duration[j].info.destinationIcao = _data.icao
								objS[i].duration[j].info.destinationName = _data.cityName
							}
						}

					}
				}
				if(angular.isDefined(objS[i].documents.image_gallary)){
					if(objS[i].documents.image_gallary.length > 0){
						for (var j = 0; j < objS[i].documents.image_gallary.length; j++) {
							objS[i].documents.image_gallary[j] = baseUrl + '/' + objS[i].documents.image_gallary[j]
						}
					}else{
						objS[i].documents.image_gallary = ['assets/img/default-slide.jpg']
					}
				}else{
					objS[i].documents.image_gallary = ['assets/img/default-slide.jpg']
				}
			}

			$scope.searchResult = objS

			console.log($scope.searchResult)

			// console.log('searchResult',$scope.searchResult)
		},function(objE){
			console.log('objE==>', objE)
		})
	}

	$scope.bookingInquiry = function(d){
		$scope.isBookingInquiry = true
		localStorage.aircraft = JSON.stringify(d)
	}

}])