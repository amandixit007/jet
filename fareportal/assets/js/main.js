$(document).ready(function(){
	// WHAT WE DO Equal Height
	var mod_height = $('.custom-about-content').parent().height();
	$('.custom-about-image').css('height', mod_height);

	// Our Investors Toggle Module
	$('.custom-inverstor-member-section').hide();
	$('.member-toggle ul li.our-investor-tgl').click(function(){
		$(this).addClass('active');
		$(this).siblings().removeClass('active');
		$('.custom-team-member-section').hide();	
		$('.custom-inverstor-member-section').fadeIn();
	});
	$('.member-toggle ul li.our-team-tgl').click(function(){
		$(this).addClass('active');
		$(this).siblings().removeClass('active');
		$('.custom-inverstor-member-section').hide();	
		$('.custom-team-member-section').fadeIn();	
	});
	// Menu Toggle
	$('a.mobile-tgl').click(function(){
		$('body').toggleClass('active-menu');
	});
	$('a.mobile-tgl, ul.menu').click(function(e){
		e.stopPropagation();	
	});
	$('body').click(function(){
		$('body').removeClass('active-menu');
	});
});

// Headroom
/*$(document).ready(function() {
    var myElement = new Headroom(document.querySelector(".headroom"), {
        offset : 100
    });
    myElement.init();
});
*/

/* Tag line */
$(function () {
	var index = 1,
		max = $('.header-tag li span').length;

	function setActive() {
		setTimeout(function () {
			$('.active-rotate').removeClass('active-rotate');
			index++;
			if(index > max) {
				index = 1;    
			}
			$('.header-tag li span:nth-of-type(' + index + ')').addClass('active-rotate');
			setActive();
		}, 2500);
	}
	
	setActive();
	
});

/* Faqs */
$(document).ready(function(){
	$('.faq p').slideUp();
	$('.faq').click(function(){
	    $(this).children('p').slideDown();
	    $(this).css('border-color', '#ee4e0c');
	    $(this).children().children('i').css('color', '#ee4e0c');
		$(this).parent().siblings().children().children('p').slideUp();
		$(this).parent().siblings().children().css('border-color', '#0f1626');
	    $(this).parent().siblings().children().children().children('i').css('color', '#0f1626');
	});
});

/* Tabs */
$(document).ready(function(){
	$('ul.tabs li').click(function(){
		var tab_id = $(this).attr('data-tab');
		$('ul.tabs li').removeClass('current');
		$('.tab-content').removeClass('current');
		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
	})
});

// Booking Toggle
$(document).ready(function(){
	$('a.booking-tgl, .modify-btn a.btn').click(function(e){
		$('.booking-module-wrapper').toggleClass('active-booking');
		e.stopPropagation();
	});
	$('.booking-area').click(function(e){
		e.stopPropagation();
	});
	$('a.close-booking-tgl, body').click(function(){
		$('.booking-module-wrapper').removeClass('active-booking');
	});
});

// datepicker
$(document).ready(function(){
	$('#date, #returndate').bootstrapMaterialDatePicker({
		time: false,
		clearButton: true
	});
	$('#time, #returntime').bootstrapMaterialDatePicker({
		date: false,
		shortTime: true,
		format: 'HH:mm'
	});
});

// slidedown
$(document).ready(function(){
	$('.return-date-wrapper').slideUp();
	$('input#roundTrip + label').click(function(){
       $('.return-date-wrapper').slideDown();
	});
	$('input#oneWay + label').click(function(){
       $('.return-date-wrapper').slideUp();
	});
});

// Filter
$(document).ready(function(){
	$('a.filter-tgl').click(function(){
		$('body').toggleClass('active-filter');
	});
	$('.filter-wrappper, .info-filter-btn a').click(function(e){
		e.stopPropagation();
	});
	$('body').click(function(){
		$('body').removeClass('active-filter');
	});
});

// flights Image
$(document).ready(function(){
	$('.flights-image .owl-item').each(function(){
		var bgimg = $(this).children().children('img').attr('src');
		$(this).children('.item').css('background-image', 'url('+ bgimg +')');
		$(this).children().children('img').remove();
	});
});

// Flights details
$(document).ready(function(){
	$('.flights-details h5 + a.more-info-tgl.btn').click(function(){
		$(this).parents('.available-flights-wrapper').toggleClass('active-details');
		$(this).parents('.available-flights-wrapper').children().children('.flight-fare').slideToggle();
	});
	$('p.close-flights-details').click(function(){
		$(this).parents('.available-flights-wrapper').removeClass('active-details');
		$(this).parents('.available-flights-wrapper').children().children('.flight-fare').slideDown();
	});
});