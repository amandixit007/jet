
// Gateway Controller //
var config=require.main.require('./global/config');
var jwt = require('jsonwebtoken');
var countrySchema=require.main.require('./db/countrySchema');
var regionSchema=require.main.require('./db/regionSchema');
var aircraftType = require.main.require('./db/aircraftTypeSchema');
var user = require.main.require('./db/userSchema.js')
var agent = require.main.require('./db/agentSchema.js')
var book_list = require.main.require('./db/passengerBookingSchema.js')
var async=require("async");
var mongoXlsx = require('mongo-xlsx');

var controller={
    addUpdateCounty:function(req,res){
    	console.log(req.body);
    	if(!req.body._id){
    		console.log("if--------->")
    		var objSchema=new countrySchema(req.body)
    		objSchema.save(function(error,result){
    			error?res.status(500).send(error):res.send(result)
			})
    	}
    	else{
			countrySchema.findByIdAndUpdate(req.body._id, {$set:req.body}, function(error,result) {
    		if(error) { throw error; }
    		error?res.status(500).send(error):res.send(result)
			    //...
			})
    	}
    },
    getAllCountries:function(req,res){
    	var obj={}
    	countrySchema.find(obj,function(err,result){
    		err?res.status(500).send(err):res.send(result);
    	})
    },
    addUpdateRegion:function(req,res){
        console.log(req.body);
        if(!req.body._id){
            console.log("if--------->",req.body)
            console.log("req.body.country--------->",req.body.country)
            console.log("typeof--------->",typeof(req.body.country))
            var objSchema=new regionSchema(req.body)
            objSchema.save(function(error,result){
                error?res.status(500).send(error):res.send(result)
            })
        }
        else{
            regionSchema.findByIdAndUpdate(req.body._id, {$set:req.body}, function(error,result) {
            if(error) { throw error; }
            error?res.status(500).send(error):res.send(result)
                //...
            })
        }
    },
    getAllRegions:function(req,res){
        var obj={}
        regionSchema.find(obj).populate('country').exec(function(err,result){
            err?res.status(500).send(err):res.send(result);
        })
    },
    addUpdateStates:function(req,res){
        console.log("req.body-------->",req.body)
        //{$push: {'list.$.items': {id: 5, name: 'item5'}}}
        // console.log("requestbody--->",req.body);
        // console.log("\n\n\n\n\ncountry --->",req.body.country);
        // var _state={stateName:req.body.StateName,region:req.body.region,createdBy:req.body.createdBy}
        // console.log("\n\n\n\n\ncountry id--->",req.body.country._id);
        // console.log("\n\n\n\n\n_state--->",_state);
        // countrySchema.update({_id:req.body.country._id}, {$addToSet:{'states':_state}}, function(error,result) {
        //     if(error) { throw error; }
        //     error?res.status(500).send(error):res.send(result)
        //         //...
        //     })
        // if(!req.body._id){
        //     console.log("if--------->",req.body)
        //     console.log("req.body.country--------->",req.body.country)
        //     console.log("typeof--------->",typeof(req.body.country))
        //     var objSchema=new regionSchema(req.body)
        //     objSchema.save(function(error,result){
        //         error?res.status(500).send(error):res.send(result)
        //     })
        // }
        // else{
        //     regionSchema.findByIdAndUpdate(req.body._id, {$set:req.body}, function(error,result) {
        //     if(error) { throw error; }
        //     error?res.status(500).send(error):res.send(result)
        //         //...
        //     })
        // }
       if(!req.body.states._id){
              countrySchema.findOneAndUpdate({
            countryName: req.body.country.countryName
        }, {
            "$push": {
                "states": {
                    "stateName": req.body.states.stateName,
                    "region":req.body.region._id,
                    "createdBy":req.body.createdBy

                }
            }
        },{new:true}, function (err, result) {
            console.log("result" + JSON.stringify(result));
              err?res.status(500).send(err):res.send(result);
        })
       }
       else{
         countrySchema.update({'states._id':req.body.states_id},{$set:{'states.$.stateName':req.body.StateName,'states.$.region':req.body.region,'states.$.createdBy':req.body.createdBy}},function(err,result){
            // countrySchema.update({'states._id':req.body.states_id},{$set:{'states':{req.body}}},function(err,result){
            err?res.status(500).send(err):res.send(result);

        })

       }

    },
    getAllStates:function(req,res){
        console.log("re");

        var obj={}
        // countrySchema.aggregate([{ 
        //     $unwind : "$states" 
        //     },{
        //         "$lookup":{
        //             "from":"region",
        //             "localField":"states",
        //             "foreignField":"states.region",
        //             "as":"region_name"
        //         }
        //     }
        //     // { "$unwind": "$region_name" },
        //     // { "$group": {
        //     //     "_id": "$_id",
        //     //     "states": { "$push": "$states" },
        //     //     "region_name": { "$push": "$region_name" }
        //     // }}
        // ] ).exec(function(err,result){
        //     //region
        //     err?res.status(500).send(err):res.send(result);
        // })




          countrySchema.aggregate([{
                            $unwind: '$states'
                        }])
            .exec(function(err, transactions) {
                regionSchema.populate(transactions, {path: 'states.region'}, function(err, result) {
                   err?res.status(500).send(err):res.send(result);
                });
            });

    },

    addCity: function (req, res) {
        console.log("request" + JSON.stringify(req.body));
     if(!req.body.city_id){
        countrySchema.update({
            countryName: req.body.countryName,
            'states._id': req.body.states_id
        }, {
            "$push": {
                "states.$.cities": {
                    "cityName": req.body.cityName,
                    "cityCode":req.body.cityCode,
                    "createdBy":req.body.createdBy
                }
            }
        }, function (err, result) {
            console.log("result" + JSON.stringify(result));
              err?res.status(500).send(err):res.send(result);
        })
    }
    else{
        console.log("request"+JSON.stringify(req.body));
   var cityname = req.body.cityName;
   var cityCode = req.body.cityCode
        countrySchema.findOne({            
            countryName: req.body.countryName
        }, function (err, result) {
            console.log("result",result.states[0]);
            if (err) { 
              return res.send({status:500,message:"something went wrong"})
            } else {             
                result.states.id(req.body.states_id).cities.id(req.body.city_id).cityName = cityname
                 result.states.id(req.body.states_id).cities.id(req.body.city_id).cityCode = cityCode
                result.save(function (err, result2) {
                    err?res.status(500).send(err):res.send(result2);

                })
            }
        })
    }

    },

    
    viewCity : function(req,res){
        console.log("reqqq");

        countrySchema.aggregate([{
            "$unwind": "$states"
    }, {
            "$unwind": "$states.cities"
    // },{ "$project" : { states: 1} }], function (err, result) {
    }], function (err, result) {
            console.log("result" + JSON.stringify(result));
               err?res.status(500).send(err):res.send(result);

        })
    },

    'updateCity' : function(req,res){
   console.log("request"+JSON.stringify(req.body));
   var cityname = req.body.cityName;
   var cityCode = req.body.cityCode
        countrySchema.findOne({            
            countryName: req.body.countryName
        }, function (err, result) {
            console.log("result",result.states[0]);
            if (err) { 
              return res.send({status:500,message:"something went wrong"})
            } else {             
                result.states.id(req.body.states_id).cities.id(req.body.city_id).cityName = cityname
                 result.states.id(req.body.states_id).cities.id(req.body.city_id).cityCode = cityCode
                result.save(function (err, result2) {
                    err?res.status(500).send(err):res.send(result2);

                })
            }
        })

    },

    'addUpdateAircraftType': function(req,res){
        console.log("req",req.body);
        if(!req.body._id){
         var aircraftType_save = new aircraftType(req.body);
         aircraftType_save.save(function(error,result){
              error?res.status(500).send(error):res.send(result)
         })
        }
        else{
         aircraftType.findByIdAndUpdate(req.body._id, {$set:req.body}, function(error,result) {
            if(error) { throw error; }
            error?res.status(500).send(error):res.send(result)
                //...
            })

        
        }

    },

    all_user_list : function(req,res){
     user.find({status:"active"},function(err,result){
        err?res.status(500).send(err):res.send(result)

     })

    },


    view_user_detail : function(req,res){
    console.log("req",req.body)
    user.findOne({_id:req.body._id},function(err,result){
 err?res.status(500).send(err):res.send(result)

    })


    },

    all_booking_list : function(req,res){
    book_list.find({},function(err,result){
 err?res.status(500).send(err):res.send(result)

    })

    },

    booking_list_detail:function(req,res){
        console.log("req",req.body)
        book_list.findOne({_id:req.body._id}).populate({path:'aircraftId',select:'jet_name aircraft_type'}).exec(function(err,result){

 err?res.status(500).send(err):res.send(result)

        })
    },



    'getAircraftType' : function(req,res){
        aircraftType.find({},function(err,result){
                err?res.status(500).send(err):res.send(result)
        })

    },
    detailAircrafttype:function(req,res){
        aircraftType.findOne({_id:req.body._id},function(err,result){
               err?res.status(500).send(err):res.send(result)
        })
    },

    test:function(req,res){

        countrySchema.findOne({countryName:"India"},function(err,result){
          for(var i=0;i<result.states.length;i++){
         countrySchema.update({countryName:"India","states._id":result.states[i]._id},{$set:{"states.$.cities":[]}},function(err,result2){
      console.log("result",result2)

         })

          }


        })
    },

    Number_of_counts : function(req,res){
        async.parallel({
    user: function(callback) {
        user.count({},function(err,result){
             callback(null, result);
        })
       
    },
    agent: function(callback) {
        agent.count({},function(err,result1){
             callback(null, result1);
        })
       
    },
    bookingCount : function(callback){
        book_list.count({},function(err,result2){
                 callback(null, result2);
        })
    }
}, function(err, results) {
    console.log("results",results)
      err?res.status(500).send(err):res.send({result:results})
    // results now equals to: [one: 'abc\n', two: 'xyz\n']
});
    }





}

module.exports = controller;


//   var data = [ { countryName : "INDiaaa", countryCode : "Ind", currencyCode : "INR" } , 
//              { countryName : "America", countryCode : "USA", currencyCode : "Doller"},
//              { countryName : "England", countryCode : "En", currencyCode : "Doller"}];
 
//  Generate automatic model for processing (A static model should be used) 
// var model = mongoXlsx.buildDynamicModel(data);
 
// /* Generate Excel */
// countrySchema.mongoData2Xlsx(data, model, function(err, data) {
//   console.log('File saved at:', data.fullPath); 
// });
