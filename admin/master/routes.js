// admin routes//

var express = require('express'); 
var path=require('path');   
var router = express.Router();  
var authorisation = require('../authorisation');
var controller = require('./controller');

router.post('/country',controller.addUpdateCounty)
router.get('/country',controller.getAllCountries)

router.post('/region',controller.addUpdateRegion)
router.get('/region',controller.getAllRegions)

router.post('/states',controller.addUpdateStates)
router.get('/states',controller.getAllStates)

router.post('/city',controller.addCity);
router.get('/city',controller.viewCity);
router.post('/updateCity',controller.updateCity);

router.post('/type',controller.addUpdateAircraftType);
router.get('/type',controller.getAircraftType);
router.post('/type',controller.detailAircrafttype);

router.get('/all_user_list',controller.all_user_list);
router.post('/view_user_detail',controller.view_user_detail);
router.get('/all_booking_list',controller.all_booking_list);
router.post('/booking_list_detail',controller.booking_list_detail);
router.get('/Number_of_counts',controller.Number_of_counts)

router.get('/test',controller.test);





// router.get('/getAddresses',authorisation.isAuthenticate,controller.getAddresses)
// router.post('/distributeGas',authorisation.isAuthenticate,controller.distributeGas)
// router.post('/moveToMaster',authorisation.isAuthenticate,controller.moveToMaster)


// router.get('/',authorisation.);  

module.exports = router;  