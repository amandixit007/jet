var config=require.main.require('./global/config');
var xmlCsvParser=require.main.require('./global/xmlCsvParser');
var aircraft=require.main.require('./db/aircraftSchema.js');
var agent = require.main.require('./db/agentSchema.js');
var count = require.main.require('./db/getCountschema.js');
var country = require.main.require('./db/countrySchema.js');

var waterfall = require('async-waterfall');
var mongoose = require('mongoose');
var https = require('https');
var async = require('async');





var agent_api = {
startBulkUpload:function(req,res){
  console.log(req.body)
  async.waterfall([function(cb){
    xmlCsvParser.parse(req.body.filesPath)
    .then(function(objS){
      cb(null,objS)
    },function(objE){
      cb(objE,null)
    })
  },function(data,cb){
    console.log("data---------------------->")
    console.log(data)
    var records=data["List"];
    async.map(records,function(x,cbMap){
      if(!x.hasOwnProperty("IATA Number"))
        cbMap(null,{msg:"IATA NUMBER IS MISSING."})
      var obj={
        agent_name:x["Agent Name"],
        agency_number :x["IATA Number"],
        agency_name:x["Agency"],
        address1:x["Adress 1"],
        address2:x["Adress 2"],
        city:x["City"],
        state:x["State"],
        country:x["Country"],
        pincode:x["pin Code"],
        phoneNo1:x["Phone 1"],
        phoneNo2:x["Phone 2"],
        mobileNo:x["Mobile"],
        email_id:x["Email id"],
        assigned_sales_person:x["assigned_sales_person"],
        faxNo:x["faxNo"]
      }
      var _agent=new agent(obj);
      _agent.save(function(err,response){
        err?cbMap(err,null):cbMap(null,response)
      })
    },function(err,result){
      err?cb(err,null):cb(null,result);
    })
  }],function(err,result){
    err?res.status(500).send(err):res.send(result)
  })
  
  //res.send(req.body);
},


'createUpdateAgent':function(req,res){
    console.log("request",req.body);
    if(!req.body._id){
      agent.find({agency_number:req.body.agency_number},function(err,result){
         if(err){res.send(500).send(err)}
          else if(result.length != 0){
             res.send(404).send("Agency Number is already exist")
          }
          else{
             var agent_save = new agent(req.body);
          agent_save.save(function(err,result){
          err?res.status(500).send(err):res.send(result)
            })
          }


      })

   
  }
    else{
      agent.findByIdAndUpdate({_id:req.body._id},{$set:req.body},function(err,result){
       if(err) { return res.send({status:500,message:"something went wrong"})}
                err?res.status(500).send(err):res.send(result)

        })
    }

//   if(!req.body._id){
//    waterfall([
//                (callback)=>{
//                  get_count({IATA_count:1},callback);
//                },
//                (iata_count,callback)=>{
//                  console.log('order_count-->'+iata_count);

//                      var str = req.body.agent_name;
//                       var res = str.substring(0, 2);
//                     req.body.agency_number = iata_count.IATA_count+res;
//                     var agent_save = new agent(req.body);
//                    agent_save.save(function (err, result) {
//                       if(err){
//                        callback(err,null)
//                       }
//                       else{
//                       callback(null,result);
//                       }
//                     });
                
//                }
               
//                ],function(err,success){
//                   err?res.status(500).send(err):res.send(success)
               
//              })
// }
// else{
//    user.findByIdAndUpdate({_id:req.body._id},{$set:req.body},function(err,result){
//        if(error) { return res.send({status:500,message:"something went wrong"})}
//                 err?res.status(500).send(err):res.send(result)

//         })




},

'viewAllagent' :function(req,res){
    agent.find({},function(err,result){
          err?res.status(500).send(err):res.send(result)
    })

    //   console.log("request-->"+JSON.stringify(req.params));

    //   var query  = {};
    // var options = {
    //       sort:     {created_at: -1 },
    //       page:   req.params.page_number, 
    //       limit:    10
    //   };
    // agent.paginate(query,options,function (err, result) {
    //   console.log("result" + JSON.stringify(result));
    //    err?res.status(500).send(err):res.send(result)

    // })
},

'agentDetail':function(req,res){
    agent.finOne({_id:req.body._id},function(err,result){
          err?res.status(500).send(err):res.send(result)
    })
},

'genrateNumber' : function(req,res){
    console.log("request--",req.body);
    if(!req.body.city_code){
        res.status(404).send("please send cityCode first")

    }
    else{
   count.findOne({city_code:req.body.city_code},function(err,result){
    console.log("result",result)
    if(err){
      res.send({response_code:500,response_message:"Server error"})
    }
    else if(!result){
          var count_save = new count(req.body)
          count_save.save(function(err,result){
          err?res.status(500).send(err):res.send(result)

          })

    }
    else{
      var b = result.count
         count.findOneAndUpdate({city_code:req.body.city_code},{$set:{count:b+1}},{new:true},function(err,result){
              err?res.status(500).send(err):res.send(result)
           })

    }

   })
    }
},

'country_list': function(req,res){
  country.find({},{countryName:1,countryCode:1,states:1},function(err,result){
     err?res.status(500).send(err):res.send(result)
  })

}


}

module.exports = agent_api;


/*function for get IATA count*/

 var get_count = function (return_object,callback) {
   waterfall([
        (cb)=>{
          count.findOne({}, return_object,function(err,success_data){
             if(err) cb(err,null);
             else cb(null,success_data);
          })
        },
        (success_data,cb)=>{
          count.findByIdAndUpdate(success_data._id ,{
            $inc:return_object
          },{ projection: return_object , new :true},function(err,updated_count){
            if(err) cb(err,null);
            else cb(null,updated_count);
          })
        }
    ],function(err,result){
      if(err){
           console.log(err);
      }else{    
        callback(null,result);        
      }
    })
  }
