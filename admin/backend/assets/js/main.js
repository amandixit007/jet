var adminPanel = angular.module('adminPanel', ['ui.router']);

adminPanel.config(function($stateProvider, $urlRouterProvider) {

    if(localStorage.token) {
        $urlRouterProvider.otherwise('/dashboard');
    }else{
        $urlRouterProvider.otherwise('/login');
    }

    $stateProvider
	    .state('login', {
	        url: '/login',
	        resolve : {
	                checkToken : function($state){
	                    if(localStorage.token){
	                        $state.go('dashboard');
	                        return true;
	                    }else{
	                    	return false;
	                    }
	                }
	            },
	        views : {
	            'mainView':{
	                templateUrl: 'views/guest/loginForm.html',
	                controller: 'login'
	            },
	        }
	    })
	    .state('dashboard', {
	    	url: '/dashboard',
	        resolve : {
	                checkToken : function($state){
	                    if(!localStorage.token){
	                        $state.go('login');
	                        return true;
	                    }else{
	                    	return false;
	                    }
	                }
	            },
	        views : {
	            'mainView':{
	                templateUrl: 'views/auth/dashboard.html',
	                controller: 'admin',
	            },
	        }
	    })
})

/////////////////////          CONTROLLER      //////////////////
adminPanel.controller('login', ['$scope', '$rootScope', '$http', '$state', function($scope, $rootScope, $http, $state){
	$scope.admin= {
		'username': '',
		'password': '',
	}
	$scope.loginAttempt = function(){
		console.log('loginAttempt');
		$http.post('http://localhost:8080/login',$scope.admin).then(function(result){
			console.log(result);
			// localStorage.token = result.data.token;
			if(result.status == 200){
				console.log('status 200');
				console.log(result.data.token);
				localStorage.token = result.data.token;
				$state.go('dashboard');
				socket.emit("initChat",{token:result.data.token,userId:result.data.userID});
				socket.on('addressDetail',function(data){
					console.log(data)
				});
			}
		})
	}
}])

adminPanel.controller('admin', ['$scope', '$rootScope', '$http', '$state', function($scope, $rootScope, $http, $state){
	
}])
