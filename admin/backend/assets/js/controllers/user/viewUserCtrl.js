(function(){
	angular.module('controllers')
	.controller('viewUserCtrl', ['$q','$scope', '$rootScope', '$http', '$state','user','customAlert','$fancyModal',"modalAnimation","master",'baseURL', function($q,$scope, $rootScope, $http, $state,user,customAlert,$fancyModal,modalAnimation,master,baseURL){

		var getUserList = function(){
			user.list().then(function(objS){
				for (var i = 0; i < objS.length; i++) {
					var _temp = objS[i]
					let date = new Date(_temp.created_at)
					var monthNames = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];
					_temp.time = (date.getDate()<10?'0'+date.getDate():date.getDate())+' '+monthNames[date.getMonth()]+' '+ date.getFullYear()+' '+ (date.getHours()>12?date.getHours()-12:date.getHours())+':'+ date.getMinutes()+(date.getHours()>12?' pm':' am')
				}
				$scope.Users = objS
				console.log($scope.Users)
			},function(objE){
				console.log(objE)
			})
		}
		getUserList()

	}])
})()