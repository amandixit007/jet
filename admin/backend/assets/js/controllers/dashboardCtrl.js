(function(){
	angular.module('controllers')
	.controller('dashboardCtrl', ['$scope', '$rootScope', 'statistics', '$http', '$state','customAlert','$fancyModal',"modalAnimation", function($scope, $rootScope, statistics, $http, $state,customAlert,$fancyModal,modalAnimation){

		statistics.all()
		.then(function(objS){
			$scope.Stact = objS.result
			console.log(objS)
		},function(objE){
			console.log(objE)
		})

		$scope.access={}
		if(angular.isDefined(localStorage.userType))
			$scope.userType = localStorage.userType

		if(angular.isDefined(localStorage.username))
			$scope.username=localStorage.username


		if(angular.isDefined(localStorage.access_level && localStorage.access_level!='')){
			$scope.access = JSON.parse(localStorage.access_level)
		}
		// console.log($scope.access)
	}])
})()
