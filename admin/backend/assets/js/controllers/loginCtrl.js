(function(){
	angular.module('controllers')
	.controller('loginCtrl', ['$scope', '$rootScope', '$http', '$state','user','customAlert', function($scope, $rootScope, $http, $state,user,customAlert){
		$scope.admin= {
			'username': '',
			'password': '',
		}
		$scope.loginAttempt = function(){
			user.login($scope.admin)
			.then(function(data){
				console.log(data)

				if(data.status == 200 ){
					var result = data.data
					console.log(result)

					localStorage.token = result.token;

					if(angular.isDefined(result.result)){
							console.log('type',result.result.type)
							localStorage.userType = result.result.type
							localStorage.username = result.result.user_name
							if(angular.isDefined(result.result.access_level)){
								localStorage.access_level = JSON.stringify(result.result.access_level)
							}
						}
						$state.go('main.dashboard');
				}

				// if(data.status == 200){
				// 	// console.log('status 200');
				// 	localStorage.token = result.token;
				// 	// localStorage.username = result.username;
				// 	if(angular.isDefined(result.result)){
				// 		console.log('type',result.result.type)
				// 		localStorage.userType = result.result.type
				//
				// 		if(angular.isDefined(result.result.access_level)){
				// 			localStorage.access_level = JSON.stringify(result.result.access_level)
				// 		}
				// 	}
				// 	$state.go('main.dashboard');
				// }
			},function(err){
				customAlert.show("Login",err.data.msg)
			});

		}
	}])
})()
