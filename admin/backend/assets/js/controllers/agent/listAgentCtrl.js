(function(){
	angular.module('controllers')
	.controller('listAgentCtrl', ['$q','$http','$scope', '$rootScope', '$state','user','customAlert','$fancyModal',"modalAnimation","master",'baseURL','agent', function($q,$http,$scope, $rootScope, $state,user,customAlert,$fancyModal,modalAnimation,master,baseURL,agent){
		// $scope.aircraft={files:[]}
  //   $q.all([aircraft.getAll()]).then(function(arr){
  //     $scope.aircrafts=arr[0];
  //   })

  //agent list
  var getAgentList = function(){
    agent.getAgentList().then(function(objS){
      $scope.agents = objS
      $scope.agents=$scope.agents.map(x=>{x.faxNo =parseInt(x.faxNo); x.phoneNo =parseInt(x.phoneNo); x.pincode =parseInt(x.pincode); x.phoneNo1 =parseInt(x.phoneNo1); return x});
    },function(objE){console.log(objE)})
  }
  getAgentList()

    $scope.newAgent = {}
    $scope.agencyNo = {}
    $scope.newAgent.owner_manegement_team = [{name:'',designation:'',date_of_birth:'',anniversery:'',phone:'',}];
    $scope.newAgent.key_personal_detail = [{name:'',designation:'',date_of_birth:'',anniversery:'',phone:'',}];
    $scope.newAgent.name_of_corparate_account = [{name:'',designation:'',date_of_birth:'',anniversery:'',phone:'',}];

      $scope.add = {
        owner_manegement_team : function(){
          var obj = {name:'',designation:'',date_of_birth:'',anniversery:'',phone:'',}
          $scope.newAgent.owner_manegement_team.push(obj);
        },
        key_personal_detail : function(){
          var obj = {name:'',designation:'',date_of_birth:'',anniversery:'',phone:'',}
          $scope.newAgent.key_personal_detail.push(obj);
        },
        name_of_corparate_account : function(){
          var obj = {name:'',designation:'',date_of_birth:'',anniversery:'',phone:'',}
          $scope.newAgent.name_of_corparate_account.push(obj);
        }
      }

      $scope.remove = {
        owner_manegement_team : function(){
          var lastItem = $scope.newAgent.owner_manegement_team.length-1;
          $scope.newAgent.owner_manegement_team.splice(lastItem)
        },
        key_personal_detail : function(){
          var lastItem = $scope.newAgent.key_personal_detail.length-1;
          $scope.newAgent.key_personal_detail.splice(lastItem)
        },
        name_of_corparate_account : function(){
          var lastItem = $scope.newAgent.name_of_corparate_account.length-1;
          $scope.newAgent.name_of_corparate_account.splice(lastItem)
        },
      }

 $scope.openModal=function(type,obj){
      console.log(obj)
      // var IndxAnimation=getIndex(prev);
      // console.log(IndxAnimation);
      switch(type){
        case "new":
        $scope.Title="Add";
        $scope.newAgent={};
        $scope.newAgent.owner_manegement_team = [{name:'',designation:'',date_of_birth:'',anniversery:'',phone:'',}];
    	$scope.newAgent.key_personal_detail = [{name:'',designation:'',date_of_birth:'',anniversery:'',phone:'',}];
    	$scope.newAgent.name_of_corparate_account = [{name:'',designation:'',date_of_birth:'',anniversery:'',phone:'',}];
        $scope.showSubmit=true;
        // $scope.new.country=$scope.Countries[0];
        // $scope.new.region=$scope.Regions[0];
        break;

        case "update":
        $scope.Title="Update";
        $scope.newAgent=obj;
        // $scope.showSubmit=true;
        // $scope.new.country=$scope.Countries.find(x=>x._id==obj._id);
        // $scope.new.region=$scope.Regions.find(x=>x._id==obj.states.region._id);
        break;

        case "view":
        $scope.Title="";
        $scope.newAgent=obj;
        // $scope.showSubmit=false;
        // $scope.new.country=$scope.Countries.find(x=>x._id==obj._id);
        // $scope.new.region=$scope.Regions.find(x=>x._id==obj.states.region._id);
        break;
      }
     $fancyModal.open({
          templateUrl: 'agent-Modal.html',
          scope: $scope,
          openingClass: 'animated '+modalAnimation[9].in,
          closingClass: 'animated '+modalAnimation[9].out,
          openingOverlayClass: 'animated fadeIn',
          closingOverlayClass: 'animated fadeOut',
        });
     //prev=IndxAnimation;
    };


		    $scope.submitForm=function(isValid){
      if(!isValid)
        return;
      agent.createUpdateAgent($scope.newAgent)
      .then(function(objS){
        // customAlert.show("Aircraft","Aircraft "+$scope.Title+" successfully.");
        console.log(objS)
        getAgentList()
        $fancyModal.close();
      },function(objE){
        customAlert.show("Error",objE.msg)
      })
    }
	}])
})()
/*

*/