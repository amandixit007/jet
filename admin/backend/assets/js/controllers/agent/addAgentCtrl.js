(function(){
	angular.module('controllers')
	.controller('addAgentCtrl', ['$q','$http','$scope', '$rootScope', '$state','user','customAlert','$fancyModal',"modalAnimation","master",'baseURL','agent','excelFormat', function($q,$http,$scope, $rootScope, $state,user,customAlert,$fancyModal,modalAnimation,master,baseURL,agent,excelFormat){
		// $scope.bulkUploadFile
    // $q.all([aircraft.getAll()]).then(function(arr){
    //   $scope.aircrafts=arr[0];
    // })

    	// $http.get('/admin/agent/country_list').then(function(r){
    	// 	$scope.country = r.data
    	// 	console.log('country',$scope.country);
    	// })

    	$scope.newAgent = {}
    	$scope.agencyNo = {}
      $scope.Companies = [
        {
          name:'IATA Agent',
          value:'143',
        },
        {
          name:'Non IATA',
          value:'91A',
        },
        {
          name:'Oerator',
          value:'91O',
        },
        {
          name:'GHA',
          value:'91G',
        },
        {
          name:'Catering',
          value:'91G',
        },
        {
          name:'Security',
          value:'91S',
        },
        {
          name:'Fuel',
          value:'91F',
        },
      ]
    	$scope.newAgent.owner_manegement_team = [{name:'',designation:'',date_of_birth:'',anniversery:'',phone:'',}];
    	$scope.newAgent.key_personal_detail = [{name:'',designation:'',date_of_birth:'',anniversery:'',phone:'',}];
    	$scope.newAgent.name_of_corparate_account = [{name:'',designation:'',date_of_birth:'',anniversery:'',phone:'',}];

    	$scope.add = {
    		owner_manegement_team : function(){
    			var obj = {name:'',designation:'',date_of_birth:'',anniversery:'',phone:'',}
    			$scope.newAgent.owner_manegement_team.push(obj);
    		},
    		key_personal_detail : function(){
    			var obj = {name:'',designation:'',date_of_birth:'',anniversery:'',phone:'',}
    			$scope.newAgent.key_personal_detail.push(obj);
    		},
    		name_of_corparate_account : function(){
    			var obj = {name:'',designation:'',date_of_birth:'',anniversery:'',phone:'',}
    			$scope.newAgent.name_of_corparate_account.push(obj);
    		}
    	}

    	$scope.remove = {
    		owner_manegement_team : function(){
    			var lastItem = $scope.newAgent.owner_manegement_team.length-1;
    			$scope.newAgent.owner_manegement_team.splice(lastItem)
    		},
    		key_personal_detail : function(){
    			var lastItem = $scope.newAgent.key_personal_detail.length-1;
    			$scope.newAgent.key_personal_detail.splice(lastItem)
    		},
    		name_of_corparate_account : function(){
    			var lastItem = $scope.newAgent.name_of_corparate_account.length-1;
    			$scope.newAgent.name_of_corparate_account.splice(lastItem)
    		},
    	}

		$scope.$watch('newAgent.country', function(newValue, oldValue) {
    		if(newValue == oldValue){return}
    		$scope.states = $scope.country[$scope.country.findIndex(i => i.countryName==$scope.newAgent.country)].states
    		console.log($scope.states)
    		if($scope.type.selector == 'agency'){
    			$scope.agencyNo.one = '91A'
    			// $scope.agencyNo.one = $scope.country[$scope.country.findIndex(i => i.countryName==$scope.newAgent.country)].countryCode 
    		}
    	});

    	$scope.$watch('newAgent.state', function(newValue, oldValue) {
    		if(newValue == oldValue){return}
    		console.log($scope.newAgent.state)
    		console.log($scope.states.findIndex(i => i.stateName==$scope.newAgent.state))
    		$scope.cities = $scope.states[$scope.states.findIndex(i => i.stateName==$scope.newAgent.state)].cities
    		console.log($scope.cities)
    	});

    	$scope.$watch('newAgent.city', function(newValue, oldValue) {
    		if(newValue == oldValue){return}
    		agent.generateAgencyCode({city_code : newValue}).then(function(objS){
    			console.log(objS)
    			$scope.agencyNo.two = objS.count;
    		},function(objE){
    			console.log(objE)
    		})
    	});

		
		$scope.uploadFile = function(){
           // $scope.aircraft.files=$('#filesAirCraft').prop('files');
           var _file=$('#filesAgent').prop('files');
           // console.log(_file)
           var arrFile=_file[0].name.split('.');
           var _ext=arrFile[arrFile.length-1]
           if(excelFormat.indexOf(_ext)==-1){
            customAlert.show("Agent","File format is not supportable")
            return;
           }
           $scope.isFileUploading=true;
           agent.bulkUpload(_file)
           .then(function(objS){
            console.log(objS);
            return agent.startBulkUpload(objS.file)
           	// $scope.aircraft.files=$scope.aircraft.files.concat(objS)
           	
           },function(objE){
           	$scope.isFileUploading=false;
           	customAlert("Files Upload","Error in file uploading.")
           })
           // agent.startBulkUpload("temp/1505902151066.xlsx")
           .then(function(objS){
           	$scope.isFileUploading=false;
            console.log(objS);
           },function(objE){
           	$scope.isFileUploading=false;
            console.log(objE);
           })
        };
        // $scope.removeFile=function(file){
        // 	aircraft.deleteAircraftImg(file)
        // 	.then(function(objS){

        // 	},function(objE){
        // 		console.log("Error--->",objE)
        // 	})
        // };
		$scope.submitForm=function(isValid){
			if(!isValid)
				return;
			agent.createUpdateAgent($scope.newAgent)
			.then(function(objS){
				// customAlert.show("Aircraft","Aircraft "+$scope.Title+" successfully.");
				console.log(objS)
				$fancyModal.close();
			},function(objE){
				customAlert.show("Country",objE.msg)
			})
		}



	}])
})()
/*

*/