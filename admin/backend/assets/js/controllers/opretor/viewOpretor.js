(function(){
	angular.module('controllers')
	.controller('viewOpretorCtrl', ['$q','$scope', '$rootScope', '$http', '$state','opretor','customAlert','$fancyModal',"modalAnimation","master",'baseURL', function($q,$scope, $rootScope, $http, $state,opretor,customAlert,$fancyModal,modalAnimation,master,baseURL){

		var getUserList = function(){
			opretor.list().then(function(objS){
				if(angular.isDefined(objS.result))
					$scope.userList = objS.result.map(x=>{x.pin_code =parseInt(x.pin_code); return x});
				console.log($scope.userList)
			},function(objE){
				console.log(objE)
			})
		}
		getUserList()

		$scope.deleteUser = function(id){
			opretor.delete(id)
			.then(function(objS){
				customAlert.show('success',objS.response_message)
				getUserList()
			},function(objE){
				console.log(objE)
			})
		}

		$scope.openModal=function(type,obj){

		  switch(type){
		    case "new":
		    $scope.type = type;
		    $scope.Title="Add";
		    $scope.newUser={};
		    $scope.showSubmit=true;
		    break;

		    case "update":
		    $scope.type = type;
		    $scope.Title="Update";
		    $scope.newUser=obj;
		    $scope.showSubmit=true;
		    break;

		    case "view":
		    $scope.type = type;
		    $scope.Title="";
		    $scope.newUser=obj;
		    $scope.showSubmit=false;
		    break;
		  }
		 $fancyModal.open({
		      templateUrl: 'user-Modal.html',
		      scope: $scope,
		      openingClass: 'animated '+modalAnimation[9].in,
		      closingClass: 'animated '+modalAnimation[9].out,
		      openingOverlayClass: 'animated fadeIn',
		      closingOverlayClass: 'animated fadeOut',
		    });
    	};

    	$scope.submitForm = function(valid){
			if(!valid)
				return
			opretor.edit($scope.newUser)
			.then(function(objS){
				console.log(objS)
				customAlert.show('success',objS.response_message)
				$fancyModal.close();
			},function(objE){
				console.log(objE)
			})
		}

	}])
})()