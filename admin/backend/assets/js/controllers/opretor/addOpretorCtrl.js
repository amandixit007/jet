
(function(){
	angular.module('controllers')
	.controller('addOpretorCtrl', ['$q','$scope', '$rootScope', '$http', '$state','opretor','customAlert','$fancyModal',"modalAnimation","master",'baseURL', function($q,$scope, $rootScope, $http, $state,opretor,customAlert,$fancyModal,modalAnimation,master,baseURL){
		console.log('addUserCtrl')

		$scope.newUser = {}

		$scope.submitForm = function(valid){
			if(!valid)
				return
			opretor.add($scope.newUser)
			.then(function(objS){
				console.log(objS)
				if(objS.status == 200){
					$scope.newUser = {}
					customAlert.show('Success',objS.data.response_message)
				}
				if(objS.status == 404)
					customAlert.show('Error',objS.data.msg)
			},function(objE){
				console.log(objE)
			})
		}
	}])
})()
/*

*/