(function(){
	angular.module('controllers')
	.controller('cityCtrl', ['$q','$scope', '$rootScope', '$http', '$state','user','customAlert','$fancyModal',"modalAnimation","master", function($q,$scope, $rootScope, $http, $state,user,customAlert,$fancyModal,modalAnimation,master){
		$scope.new={};
		
		$q.all([master.states(),master.countries(),master.cities()])
		.then(function(arr){
			$scope.States=arr[0];
			$scope.Countries=arr[1];
			$scope.cities=arr[2];
		},function(objE){
			console.log(objE)
		})
		
		$scope.openModal=function(type,obj){
			
			// var IndxAnimation=getIndex(prev);
			// console.log(IndxAnimation);
			switch(type){
				case "new":
				$scope.Title="Add";
				$scope.new={};
				$scope.showSubmit=true;
				$scope.new.country=$scope.Countries[0];
				$scope.new.state=$scope.States[0];
				break;

				case "update":
				$scope.Title="Update";
				$scope.new=obj;
				$scope.showSubmit=true;
				$scope.new.country=$scope.Countries.find(x=>x._id==obj._id);
				$scope.new.state=$scope.new.country.states.find(x=>x._id==obj.states._id);
				break;

				case "view":
				$scope.Title="";
				$scope.new=obj;
				$scope.showSubmit=false;
				$scope.new.country=$scope.Countries.find(x=>x._id==obj._id);
				$scope.new.state=$scope.new.country.states.find(x=>x._id==obj.states._id);
				break;
			}
		 $fancyModal.open({
	        templateUrl: 'state-Modal.html',
	        scope: $scope,
	        openingClass: 'animated '+modalAnimation[9].in,
	        closingClass: 'animated '+modalAnimation[9].out,
	        openingOverlayClass: 'animated fadeIn',
	        closingOverlayClass: 'animated fadeOut',
	      });
		 //prev=IndxAnimation;
		};	

		$scope.submitForm=function(isValid){
			if(!isValid)
				return;
			var obj={
				countryName:$scope.new.country.countryName,
				states_id:$scope.new.state._id,
				cityName:$scope.new.states.cities.cityName,
				cityCode:$scope.new.states.cities.cityCode
			};
			master.saveUpdateCity(obj)
			.then(function(objS){
				customAlert.show("City","City "+$scope.Title+" successfully.");
				$fancyModal.close();
				master.cities().then(function(arr){$scope.cities=arr});
			},function(objE){
				customAlert.show("City",objE.msg)
			})
		}
	}])
})()
/*

*/