(function(){
	angular.module('controllers')
	.controller('stateCtrl', ['$q','$scope', '$rootScope', '$http', '$state','user','customAlert','$fancyModal',"modalAnimation","master", function($q,$scope, $rootScope, $http, $state,user,customAlert,$fancyModal,modalAnimation,master){
		$scope.new={};
		
		$q.all([master.states(),master.countries(),master.regions()])
		.then(function(arr){
			$scope.States=arr[0];
			$scope.Countries=arr[1];
			$scope.Regions=arr[2];
		},function(objE){
			console.log(objE)
		})
		
		$scope.openModal=function(type,obj){
			
			// var IndxAnimation=getIndex(prev);
			// console.log(IndxAnimation);
			switch(type){
				case "new":
				$scope.Title="Add";
				$scope.new={};
				$scope.showSubmit=true;
				$scope.new.country=$scope.Countries[0];
				$scope.new.region=$scope.Regions[0];
				break;

				case "update":
				$scope.Title="Update";
				$scope.new=obj;
				$scope.showSubmit=true;
				$scope.new.country=$scope.Countries.find(x=>x._id==obj._id);
				// $scope.new.region=$scope.Regions.find(x=>x._id==obj.states.region._id);
				break;

				case "view":
				$scope.Title="";
				$scope.new=obj;
				$scope.showSubmit=false;
				$scope.new.country=$scope.Countries.find(x=>x._id==obj._id);
				// $scope.new.region=$scope.Regions.find(x=>x._id==obj.states.region._id);
				break;
			}
		 $fancyModal.open({
	        templateUrl: 'state-Modal.html',
	        scope: $scope,
	        openingClass: 'animated '+modalAnimation[9].in,
	        closingClass: 'animated '+modalAnimation[9].out,
	        openingOverlayClass: 'animated fadeIn',
	        closingOverlayClass: 'animated fadeOut',
	      });
		 //prev=IndxAnimation;
		};	

		$scope.submitForm=function(isValid){
			if(!isValid)
				return;
			master.saveUpdateState($scope.new)
			.then(function(objS){
				customAlert.show("States","State "+$scope.Title+" successfully.");
				$fancyModal.close();
				master.states().then(function(arr){$scope.States=arr});
			},function(objE){
				customAlert.show("Region",objE.msg)
			})
		}
	}])
})()
/*

*/