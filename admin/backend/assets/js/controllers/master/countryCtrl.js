(function(){
	angular.module('controllers')
	.controller('countryCtrl', ['$scope', '$rootScope', '$http', '$state','user','customAlert','$fancyModal',"modalAnimation","master", function($scope, $rootScope, $http, $state,user,customAlert,$fancyModal,modalAnimation,master){
		$scope.new={};
		$scope.Countries=[];
		var getCountries=function(){
			master.countries().then(function(countries){
				$scope.Countries=countries;
			},function(){})
		}
		getCountries()
		$scope.openModal=function(type,obj){
			
			// var IndxAnimation=getIndex(prev);
			// console.log(IndxAnimation);
			switch(type){
				case "new":
				$scope.Title="Add";
				$scope.new={};
				$scope.showSubmit=true;
				break;

				case "update":
				$scope.Title="Update";
				$scope.new=obj;
				$scope.showSubmit=true;
				break;

				case "view":
				$scope.Title="";
				$scope.new=obj;
				$scope.showSubmit=false;
				break;
			}
		 $fancyModal.open({
	        templateUrl: 'country-Modal.html',
	        scope: $scope,
	        openingClass: 'animated '+modalAnimation[9].in,
	        closingClass: 'animated '+modalAnimation[9].out,
	        openingOverlayClass: 'animated fadeIn',
	        closingOverlayClass: 'animated fadeOut',
	      });
		 //prev=IndxAnimation;
		};	

		$scope.submitForm=function(isValid){
			if(!isValid)
				return;
			master.saveUpdateCountry($scope.new)
			.then(function(objS){
				customAlert.show("Country","Country "+$scope.Title+" successfully.");
				$fancyModal.close();
				getCountries();
			},function(objE){
				customAlert.show("Country",objE.msg)
			})
		}
	}])
})()
/*

*/