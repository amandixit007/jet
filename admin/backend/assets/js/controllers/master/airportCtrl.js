(function(){
	angular.module('controllers')
	.controller('airportCtrl', ['$q','$scope', '$rootScope', '$http', '$state','user','customAlert','$fancyModal',"modalAnimation","master", function($q,$scope, $rootScope, $http, $state,user,customAlert,$fancyModal,modalAnimation,master){
		$scope.new={};

		$scope.Airports = []
		$scope.States = []
		$scope.Cities = []
		$scope.Countries = []

		var getAirportsList = function(){
			master.airports()
			.then(function(objS){
				var useableData = []

				for (var i = 0; i < objS.length; i++) {
					var allData = objS[i]
					/**/
					for (var j = 0; j < allData.airportList.length; j++) {
						var airport = allData.airportList[j]
						useableData.push({
							airport : airport.airport,
							stateName : allData.states.stateName,
							state : allData.states._id,
							countryName : allData.countryName,
							country : allData._id,
							cityName : allData.states.cities.cityName,
							city : allData.states.cities._id,
							iata : airport.iata,
							icao : airport.icao,
							_id : airport._id,
						})
					}
					/**/
				}

				$scope.Airports = useableData
				console.log('Airports')
				console.dir($scope.Airports)
			},function(objE){
				console.log(objE)
			})
		}
		getAirportsList()

		var getStatesList = function(){
			master.states()
			.then(function(objS){
				$scope.States = objS
				console.log('states')
				console.dir($scope.States)
			},function(objE){
				console.log(objE)
			})
		}
		getStatesList()

		var getCountriesList = function(){
			master.countries()
			.then(function(objS){
				$scope.Countries = objS
				console.log('Countries')
				console.dir($scope.Countries)
			},function(objE){
				console.log(objE)
			})
		}
		getCountriesList()

		var getCitiesList = function(){
			master.cities()
			.then(function(objS){
				$scope.Cities = objS
				console.log('Cities')
				console.dir($scope.Cities)
			},function(objE){
				console.log(objE)
			})
		}
		getCitiesList()
		
		$scope.openModal=function(type,obj){

			switch(type){
				case "new":
				$scope.Title="Add";
				$scope.new={};
				$scope.showSubmit=true;
				break;

				case "update":
				$scope.Title="Update";
				$scope.new=obj;
				$scope.showSubmit=true;
				break;

				case "view":
				$scope.Title="";
				$scope.new=obj;
				$scope.showSubmit=false;
				break;
			}
		 $fancyModal.open({
	        templateUrl: 'airport-Modal.html',
	        scope: $scope,
	        openingClass: 'animated '+modalAnimation[9].in,
	        closingClass: 'animated '+modalAnimation[9].out,
	        openingOverlayClass: 'animated fadeIn',
	        closingOverlayClass: 'animated fadeOut',
	      });
		 //prev=IndxAnimation;
		};	

		$scope.submitForm=function(isValid){
			if(!isValid)
				return;
			console.log($scope.new)
			master.saveUpdateAirport($scope.new)
			.then(function(objS){
				customAlert.show("Airport","Airport save");
				// console.log(objS)
				$fancyModal.close();
				getAirportsList()
			},function(objE){
				customAlert.show("Error",objE.msg)
			})
		}
	}])
})()
/*

*/