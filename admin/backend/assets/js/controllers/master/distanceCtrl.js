(function(){
	angular.module('controllers')
	.controller('distanceCtrl', ['$q','$scope', '$rootScope', '$http', '$state','user','customAlert','$fancyModal',"modalAnimation","master", function($q,$scope, $rootScope, $http, $state,user,customAlert,$fancyModal,modalAnimation,master){
		$scope.new={};

		$scope.airportsDistance = []
		$scope.Airports = []
		$scope.currentPage = $state.params.page_no
		$scope.limit = 10
		$scope.totalData = 10000
		$scope.showPagination = false



		var getAirportsList = function(){
			master.airports()
			.then(function(objS){
				var useableData = []
				for (var i = 0; i < objS.length; i++) {
					var allData = objS[i]
					/**/
					for (var j = 0; j < allData.airportList.length; j++) {
						var airport = allData.airportList[j]
						useableData.push({
							airport : airport.airport,
							stateName : allData.states.stateName,
							stateId : allData.states._id,
							countryName : allData.countryName,
							countryId : allData._id,
							cityName : allData.states.cities.cityName,
							cityId : allData.states.cities._id,
							iata : airport.iata,
							icao : airport.icao,
							_id : airport._id,
						})
					}
					/**/
				}
				$scope.Airports = useableData
				console.log('Airports')
				console.dir($scope.Airports)
				getAirportsDistance($scope.currentPage)
			},function(objE){
				console.log(objE)
			})
		}
		getAirportsList()

		var getAirportsDistance = function(page_no){
			master.distance(page_no)
			.then(function(objS){
				$scope.totalData = objS.pagination.total
				$scope.airportsDistance = objS.result
				for(var i=0; i < $scope.airportsDistance.length; i++){
					var tempObj1 = $scope.airportsDistance[i]
					for(var j=0; j<$scope.Airports.length; j++){
						var tempObj2 = $scope.Airports[j]
						if(tempObj1.fromAirport == tempObj2._id){
							$scope.airportsDistance[i].fromAirportName = tempObj2.airport
							$scope.airportsDistance[i].fromAirportIcao = tempObj2.icao
							$scope.airportsDistance[i].fromAirportIata = tempObj2.iata
						}
						if(tempObj1.toAirport == tempObj2._id){
							$scope.airportsDistance[i].toAirportName = tempObj2.airport
							$scope.airportsDistance[i].toAirportIcao = tempObj2.icao
							$scope.airportsDistance[i].toAirportIata = tempObj2.iata
						}
						if(angular.isDefined($scope.airportsDistance[i].fromAirportName) && angular.isDefined($scope.airportsDistance[i].toAirportName)){
							break;
						}
					}
				}
				$scope.showPagination = true;
				console.log($scope.airportsDistance)
			},function(objE){
				console.log(objE)
			})
		}
		// getAirportsDistance($scope.currentPage)
		
		$scope.openModal=function(type,obj){
			switch(type){
				case "new":
				$scope.Title="Add";
				$scope.new={};
				$scope.showSubmit=true;
				break;

				case "update":
				$scope.Title="Update";
				$scope.new=obj;
				$scope.showSubmit=true;
				break;

				case "view":
				$scope.Title="";
				$scope.new=obj;
				$scope.showSubmit=false;
				break;
			}
		 $fancyModal.open({
	        templateUrl: 'distance-Modal.html',
	        scope: $scope,
	        openingClass: 'animated '+modalAnimation[9].in,
	        closingClass: 'animated '+modalAnimation[9].out,
	        openingOverlayClass: 'animated fadeIn',
	        closingOverlayClass: 'animated fadeOut',
	      });
		 //prev=IndxAnimation;
		};	

		$scope.submitForm=function(isValid){
			if(!isValid)
				return;
			master.saveUpdateDistance($scope.new)
			.then(function(objS){
				customAlert.show("Success","Distance Save");
				$fancyModal.close();
				getAirportsDistance()
			},function(objE){
				customAlert.show("Error",objE.msg)
			})
		}

		$scope.pageChange=function(page){
			$state.go('.',{page_no:page})
		}

	}])
})()
/*

*/