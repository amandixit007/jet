(function(){
	angular.module('controllers')
	.controller('viewBookingCtrl', ['$q','$scope', '$rootScope', '$http', '$state','booking','customAlert','$fancyModal',"modalAnimation","master",'baseURL', function($q,$scope, $rootScope, $http, $state,booking,customAlert,$fancyModal,modalAnimation,master,baseURL){

		console.log('booking ctrl')

		var getBookingList = function(){
			booking.list().then(function(objS){
				for (var i = 0; i < objS.length; i++) {
					var _temp = objS[i]
					let date = new Date(_temp.created_at)
					var monthNames = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];
					_temp.time = (date.getDate()<10?'0'+date.getDate():date.getDate())+' '+monthNames[date.getMonth()]+' '+ date.getFullYear()+' '+ (date.getHours()>12?date.getHours()-12:date.getHours())+':'+ date.getMinutes()+(date.getHours()>12?' pm':' am')

					for (var j = 0; j < _temp.roots.length; j++) {
						_tempRoot = _temp.roots[j]
						let rootDate = new Date(_tempRoot.departureDate)
						_tempRoot.time = (date.getDate()<10?'0'+date.getDate():date.getDate())+' '+monthNames[date.getMonth()]+' '+ date.getFullYear()+' '+ (date.getHours()>12?date.getHours()-12:date.getHours())+':'+ date.getMinutes()+(date.getHours()>12?' pm':' am')
					}
				}
				$scope.Bookings = objS
				console.log($scope.Bookings)
			},function(objE){
				console.log(objE)
			})
		}
		getBookingList()

		$scope.openModal=function(type,obj){
			console.log(obj)

			switch(type){
				case "new":
					$scope.type = type;
					$scope.Title="Add";
					$scope.new={};
					$scope.showSubmit=true;
					break;

				case "update":
					$scope.type = type;
					$scope.Title="Update";
					$scope.new=obj;
					$scope.showSubmit=true;
					break;

				case "view":
					$scope.type = type;
					$scope.Title="";
					$scope.new=obj;
					$scope.showSubmit=false;
					break;
			}

			$fancyModal.open({
				templateUrl: 'booking-Modal.html',
				scope: $scope,
				openingClass: 'animated '+modalAnimation[9].in,
				closingClass: 'animated '+modalAnimation[9].out,
				openingOverlayClass: 'animated fadeIn',
				closingOverlayClass: 'animated fadeOut',
		    });
		};  

	}])
})()