(function(){
	angular.module('controllers')
	.controller('addAircraftCtrl', ['$http','$scope', '$rootScope', '$http', '$state','user','customAlert','$fancyModal',"modalAnimation","master",'baseURL','aircraft', function($http,$scope, $rootScope, $http, $state,user,customAlert,$fancyModal,modalAnimation,master,baseURL,aircraft){
		$scope.aircraft={files:[]}
    $scope.selectedBase={};
    var getCountries = function(){
      aircraft.getCountries()
      .then(function(objS){
        var citiesList = []
        for(var i = 0; i < objS.length; i++ ){
          var tempObj = objS[i]
          citiesList.push(tempObj.states.cities)
        }
        // console.log(citiesList)
        $scope.citiesList = citiesList
      },function(objE){
        console.log(objE)
      })
    }
    getCountries()

    $scope.newAircraft = {
      bases : []
    }

    $scope.addBase = function(){
      $scope.newAircraft.bases.push('')
    }

    $scope.removeBase = function(){
      $scope.newAircraft.bases.pop()
    }

		$scope.uploadFile = function(){
           // $scope.aircraft.files=$('#filesAirCraft').prop('files');
           $scope.isFileUploading=true;
           aircraft.uploadFiles($('#filesAirCraft').prop('files'))
           .then(function(objS){
           	$scope.isFileUploading=false;
           	$scope.aircraft.files=$scope.aircraft.files.concat(objS)
           	setTimeout(function(){
           		$('.cls-gallary').width($('.cls-gallary .gallary-img').length* ($('.cls-gallary .gallary-img').width()+20))
           	},2000)
           },function(objE){
           	$scope.isFileUploading=false;
           	customAlert("Files Upload","Error in file uploading.")
           })
           //fileUpload.uploadFileToUrl(file, uploadUrl);
           
        };
        $scope.removeFile=function(file){
        	aircraft.deleteAircraftImg(file)
        	.then(function(objS){

        	},function(objE){
        		console.log("Error--->",objE)
        	})
        };
		$scope.submitForm=function(isValid){
			if(!isValid)
				return;
      console.log($scope.newAircraft)
			aircraft.createUpdateAircraft($scope.newAircraft)
			.then(function(objS){
				// customAlert.show("Aircraft","Aircraft "+$scope.Title+" successfully.");
				$fancyModal.close();
				getCountries();
			},function(objE){
				customAlert.show("Country",objE.msg)
			})
		}
	}])
})()
/*

*/