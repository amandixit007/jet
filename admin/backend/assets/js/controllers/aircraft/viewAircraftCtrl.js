(function(){
	angular.module('controllers')
	.controller('listAircraftCtrl', ['$q','$http','$scope', '$rootScope', '$state','user','customAlert','$fancyModal',"modalAnimation","master",'baseURL','aircraft', function($q,$http,$scope, $rootScope, $state,user,customAlert,$fancyModal,modalAnimation,master,baseURL,aircraft){
		// $scope.aircraft={files:[]}
    $scope.new = {}
    $scope.new.tempImages = []

    $q.all([aircraft.getAll()]).then(function(arr){
      $scope.aircrafts=arr[0].map(x=>{x.baggage =parseInt(x.baggage); x.passangers =parseInt(x.passangers); x.pilot =parseInt(x.pilot); x.speed =parseInt(x.speed); return x});
      console.log($scope.aircrafts)
    })
		$scope.uploadFile = function(){
           // $scope.aircraft.files=$('#filesAirCraft').prop('files');

           $scope.isFileUploading=true;
           aircraft.uploadFiles($('#filesAirCraft').prop('files'))
           .then(function(objS){
            $scope.isFileUploading=false;
            // $scope.aircraft.files=$scope.aircraft.files.concat(objS)
            var tempGallary = []
            for (var i = 0; i < objS.length; i++) {
              tempGallary.push(objS[i])
            }
            $scope.new.tempImages=tempGallary
            console.log('img upload',$scope.new.tempImages)
           	setTimeout(function(){
           		$('.cls-gallary').width($('.cls-gallary .gallary-img').length* ($('.cls-gallary .gallary-img').width()+20))
           	},2000)
           },function(objE){
           	$scope.isFileUploading=false;
           	customAlert("Files Upload","Error in file uploading.")
           })
           //fileUpload.uploadFileToUrl(file, uploadUrl);
           
        };
        $scope.removeFile=function(file, index){
        	aircraft.deleteAircraftImg(file)
        	.then(function(objS){
            $scope.new.tempImages.splice(index, 1);
            console.log('removed',$scope.new.tempImages)
        	},function(objE){
        		console.log("Error--->",objE)
        	})
        };
        $scope.openModal=function(type,obj){
          // console.log(obj)
      
      // var IndxAnimation=getIndex(prev);
      // console.log(IndxAnimation);
      switch(type){
        case "new":
        $scope.type = type;
        $scope.Title="Add";
        $scope.new={};
        $scope.showSubmit=true;
        break;

        case "update":
        $scope.type = type;
        $scope.Title="Update";
        $scope.new=obj;
        $scope.showSubmit=true;
        break;

        case "view":
        $scope.type = type;
        $scope.Title="";
        $scope.new=obj;
        $scope.showSubmit=false;
        break;
      }
     $fancyModal.open({
          templateUrl: 'aircraft-Modal.html',
          scope: $scope,
          openingClass: 'animated '+modalAnimation[9].in,
          closingClass: 'animated '+modalAnimation[9].out,
          openingOverlayClass: 'animated fadeIn',
          closingOverlayClass: 'animated fadeOut',
        });
     //prev=IndxAnimation;
    };  
		$scope.submitForm=function(isValid){
			if(!isValid)
				return;
      var tempGallary = []
      for (var i = 0; i < $scope.new.tempImages.length; i++) {
        tempGallary.push($scope.new.tempImages[i].file)
      }
      $scope.new.image_gallary=tempGallary
			aircraft.createUpdateAircraft($scope.new)
			.then(function(objS){
				// customAlert.show("Aircraft","Aircraft "+$scope.Title+" successfully.");
				$fancyModal.close();
				  console.log(objS)
			},function(objE){
				customAlert.show("Country",objE.msg)
			})
		}
	}])
})()
/*

*/