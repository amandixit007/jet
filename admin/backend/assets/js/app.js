(function(){
	angular.module('controllers',[])
	angular.module('services',[])
	angular.module('filters',[])
	angular.module('directives',[])
	var externalLibs=['ui.router','ui.bootstrap','ui.bootstrap.datetimepicker','vesparny.fancyModal','directives','controllers','services','filters']
	angular.module('adminPanel', externalLibs)
	.value('baseURL','/admin/')
	// .value('excelFormat',['csv','ods','xls','xlsb','xlsb','xlsm','xlsx','xml'])
	.value('excelFormat',['csv','ods','xls','xlsx','xml'])
	.service("customAlert",['$q',function($q){
		this.show=function(title,content){
			$.alert({
		      title: title||"Remicoin",
		      content: content,
		      animation: 'rotateYR',
		      theme: 'supervan',
		      closeAnimation: 'rotateYR',
		       animationBounce: 2,
		       animationSpeed:800
		    });
		}
	}])
	.config(function($stateProvider, $urlRouterProvider, $httpProvider) {

	    $urlRouterProvider.otherwise('/login');

	    $stateProvider
		    .state('login', {
		        url: '/login',
		        views : {
		            'mainView':{
		                templateUrl: 'views/guest/loginForm.html',
		                controller: 'loginCtrl'
		            },
		        }
		    })
		    .state('main',{
		    	url: '/main',
		    	resolve:{
                	// Addresses:function($q,user){
                	// 	//return user.getAddresses();
                	// 	var deff=$q.defer();
                	// 	user.getAddresses()
                	// 	.then(function(addresses){
                	// 		deff.resolve(addresses);
                	// 	},function(error){
                	// 		deff.reject()
                	// 	})
                	// 	return deff.promise;
                	// }
                },
		        views : {
		            'mainView':{
		                templateUrl: 'views/auth/main.html',
		               controller: 'dashboardCtrl'
		            },
		        }
		    })
		    .state('main.dashboard', {
		    	url: '/dashboard',
                templateUrl: function(){
                	if(localStorage.userType == 'admin')
                		return 'views/auth/dashboard.html'
                	return 'views/auth/dashboard/operator.html'
                },
                controller: '',
		    })
		    .state('main.master', {
		    	url: '/master',
		    	abstract:true
		    })
		    .state('main.aircraft', {
		    	url: '/aircraft',
		    	abstract:true
		    })
		    .state('main.agent', {
		    	url: '/agent',
		    	abstract:true
		    })
		    .state('main.flight', {
		    	url: '/flight',
		    	abstract:true
		    })
		    .state('main.opretor', {
		    	url: '/opretor',
		    	abstract:true
		    })
		    .state('main.user', {
		    	url: '/user',
		    	abstract:true
		    })
		    .state('main.booking', {
		    	url: '/booking',
		    	abstract:true
		    })
		    /*addFlightCtrl
		    <li><a ui-sref="main.aircraft.country">Country</a></li>
				    <li><a ui-sref="main.master.region">Region</a></li>
				    <li><a ui-sref="main.aircraft.state">State</a></li>
				    <li><a ui-sref="main.aircraft.city">City</a></li>
				    <li><a ui-sref="main.aircraft.dCity">Destination City</a></li>
				    <li><a ui-sref="main.aircraft.Currency">Currency</a></li>
				    <li><a ui-sref="main.aircraft.branch">Branch</a></li>*/
			.state('main.booking.list', {
		    	url: '/list',
                templateUrl: 'views/auth/booking/list.html',
                 controller:'viewBookingCtrl'
		    })
			.state('main.user.list', {
		    	url: '/list',
                templateUrl: 'views/auth/user/list.html',
                 controller:'viewUserCtrl'
		    })
			.state('main.opretor.list', {
		    	url: '/list',
                templateUrl: 'views/auth/opretor/list.html',
                 controller:'viewOpretorCtrl'
		    })
		    .state('main.opretor.add', {
		    	url: '/add',
                templateUrl: 'views/auth/opretor/add.html',
                 controller:'addOpretorCtrl'
		    })
		    .state('main.flight.list', {
		    	url: '/list',
                templateUrl: 'views/auth/flights/list.html',
                 // controller:'listAircraftCtrl'
		    })
		    .state('main.flight.add', {
		    	url: '/add',
                templateUrl: 'views/auth/flights/add.html',
                 controller:'addFlightCtrl'
		    })
		     .state('main.aircraft.list', {
		    	url: '/list',
                templateUrl: 'views/auth/aircraft/list.html',
                 controller:'listAircraftCtrl'
		    })
		     .state('main.aircraft.add', {
		    	url: '/add',
                templateUrl: 'views/auth/aircraft/add.html',
                controller:'addAircraftCtrl'
		    })

		     .state('main.master.country', {
		    	url: '/country',
                templateUrl: 'views/auth/master/country.html',
                controller: 'countryCtrl'
		    })
		     .state('main.master.region', {
		    	url: '/region',
                templateUrl: 'views/auth/master/region.html',
                controller:'regionCtrl'
		    })
		     .state('main.master.state', {
		    	url: '/state',
                templateUrl: 'views/auth/master/state.html',
                controller:'stateCtrl'
		    })
		     .state('main.master.city', {
		    	url: '/city',
                controller:'cityCtrl',
                templateUrl: 'views/auth/master/city.html'
		    })
		     .state('main.master.dCity', {
		    	url: '/dCity',
                templateUrl: 'views/auth/master/dCity.html'
		    })
		     .state('main.master.currency', {
		    	url: '/currency',
                templateUrl: 'views/auth/master/currency.html'
		    })
		     .state('main.master.branch', {
		    	url: '/branch',
                templateUrl: 'views/auth/master/branch.html'
		    })
		     .state('main.master.airport', {
		    	url: '/airport',
                templateUrl: 'views/auth/master/airport.html',
                controller:'airportCtrl',
		    })
		     .state('main.master.distance', {
		    	url: '/distance/page/:page_no',
                templateUrl: 'views/auth/master/distance.html',
                controller:'distanceCtrl',
		    })
		     .state('main.agent.add', {
		    	url: '/add',
                templateUrl: 'views/auth/agent/add.html',
                controller:"addAgentCtrl"
		    })
		     .state('main.agent.list', {
		    	url: '/list',
                templateUrl: 'views/auth/agent/list.html',
                controller:"listAgentCtrl"
		    })


		    $httpProvider.interceptors.push('customInterceptor');
	})
	.run(function(){
		// socket.emit("initChat",{token:localStorage.token,userId:localStorage.userID});
	})
	.factory('customInterceptor',function($state,customAlert){
	   return {
	    request: function(config) {
	       config.headers['authorization'] = localStorage.token;
	      return config;
	    },

	    requestError: function(config) {
	      return config;
	    },

	    response: function(res) {
	      return res;
	    },

	    responseError: function(res) {
	       // console.log(res)
	       if(res.status==500 && res.data.msg){
	          customAlert.show("JetSmart",res.data.msg)
	          // res.data.msg?custom.alert(res.data.msg):custom.alert(res.statusText)
	          
	       }
	       if(res.status==401)
	        $state.go('login')
	      return res;
	    }
	  }
	})
	.directive('fileModel', ['$parse', function ($parse) {
            return {
               restrict: 'A',
               link: function(scope, element, attrs) {
                  var model = $parse(attrs.fileModel);
                  var modelSetter = model.assign;
                  
                  element.bind('change', function(){
                     scope.$apply(function(){
                        modelSetter(scope, element[0].files[0]);
                     });
                  });
               }
            };
    }])
	.directive("imageUploader",function(){
		return{
			restrict:"A",
			// template:'<div class="form-group" style="height: 100px;line-height: 100px;">'+
   //                      '<label class="col-md-3 control-label">Aircraft Images</label>'+
   //                      '<div class="col-md-9">'+
   //                      '<input type="file" name="file" id="file" class="inputfile" image-uploader/>'+
   //                       '<label for="file"><i class="fa fa-picture-o" aria-hidden="true"></i></label>'+
   //                       '<button ng-click>upload</button>'+
   //                       '</div></div>'+
   //                  '<div class="form-group" style="height: 100px;line-height: 100px;">'+
   //                      '<label class="col-md-3 control-label">Image Gallary</label>'+
   //                      '<div class="col-md-9 cls-gallary">'+
   //                          '<div class="gallary-img"></div>'+
   //                      '</div>'+
   //                  '</div>',
             link:function(scope,ele,attr){
             	ele.on('change',function(e){
             		console.log("change-->",e);
             		if(e.currentTarget.files.length>1){
             			$("#upload-content").html(e.currentTarget.files.length+" files are selected.")
             			$("#upload-content").next().show()

             		}
             		else if(e.currentTarget.files.length==1){
             			$("#upload-content").html(e.currentTarget.files[0].name)
             			$("#upload-content").next().show()
             		}
             	})
             	ele.on('drop', function(e) {
             		console.log("drop------!!!!!!!!!!!!!!!")
				    e.preventDefault();
				    e.stopPropagation();
				    if (e.originalEvent.dataTransfer){
				        if (e.originalEvent.dataTransfer.files.length > 0) {
				        	console.log("drop--->",e.originalEvent.dataTransfer.files);
				            //upload(e.originalEvent.dataTransfer.files);
				        }
				    }
				    return false;
				});
             },
             controller:function($scope){

             }

		};
	})
.directive('indeterminate', function() {
  return {
    // Restrict the directive so it can only be used as an attribute
    restrict: 'A',

    link: function(scope, elem, attr) {
      // Whenever the bound value of the attribute changes we update
      // the internal 'indeterminate' flag on the attached dom element
      var watcher = scope.$watch(attr.indeterminate, function(value) {
        elem[0].indeterminate = value;
      });

      // Remove the watcher when the directive is destroyed
      scope.$on('$destroy', function() {
        watcher();
      });
    }
  };
})
/*.filter("customfilter",function(){
	return function(arr,cond){
		var _arr=[];
		switch(cond){
			case "0":
			_arr=arr;
			break;

			case "1":
			_arr=arr.filter(x=>x.coinBal==0)
			break;

			case "2":
			_arr=arr.filter(x=>x.coinBal>0)
			break;

			case "3":
			_arr=arr.filter(x=>x.ethbal==0 && x.coinBal>0)
			break;

			case "4":
			_arr=arr.filter(x=>x.ethbal>0 && x.coinBal>0)
			break;

			case "5":
			_arr=arr.filter(x=>x.data.paymentStatus=="moved")
			break;
		}
		return _arr;
	}
})*/
.value("modalAnimation",[{
	in:"lightSpeedIn",
	out:"lightSpeedOut"
},{
	in:"bounceIn",
	out:"bounceOut"
},{
	in:"slideInRight",
	out:"slideOutLeft"
},{
	in:"tada",
	out:"bounceOut"
},{
	in:"fadeIn",
	out:"fadeOut"
},{
	in:"flipInX",
	out:"flipOutX"
},{
	in:"flipInY",
	out:"flipOutY"
},{
	in:"rotateIn",
	out:"rotateOut"
},{
	in:"slideInLeft",
	out:"slideOutRight"
},{
	in:"slideInUp",
	out:"slideOutUp"
}])

})()
