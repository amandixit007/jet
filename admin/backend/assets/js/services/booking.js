(function(){
	angular.module('controllers')
	.service('booking', ['$q', '$http','baseURL', function($q, $http,baseURL){

		this.list = function(){
			var deff=$q.defer();
			var url = baseURL+'master/all_booking_list'

			$http.get(url).then(function(result){
				// console.log(result)
				deff.resolve(result.data);
			},function(err){
				deff.reject(err);
			})

			return deff.promise;
		}

	}])
})()