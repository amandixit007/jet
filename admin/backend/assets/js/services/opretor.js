(function(){
	angular.module('controllers')
	.service('opretor', ['$q', '$http','baseURL', function($q, $http,baseURL){
		var self=this;

		this.add = function(data){
			var deff=$q.defer();
			
			var url ='/admin/user/user_signup'

			$http.post(url,data).then(function(result){
				deff.resolve(result);
			},function(err){
				deff.reject(err);
			})

			return deff.promise;
		};

		this.login = function(data){
			var deff=$q.defer();

			$http.post(baseURL+"oprator/login",data).then(function(result){
				deff.resolve(result);
			},function(err){
				deff.reject(err);
			})

			return deff.promise;
		};

		this.list = function(){
			var deff=$q.defer();
			
			var url = '/admin/user/user_list'

			$http.get(url).then(function(result){
				deff.resolve(result.data);
			},function(err){
				deff.reject(err);
			})

			return deff.promise;
		}

		this.delete = function(id){
			var deff=$q.defer();
			
			var url = '/admin/user/delete_user'

			$http.post(url, {user_id : id}).then(function(result){
				deff.resolve(result.data);
			},function(err){
				deff.reject(err);
			})

			return deff.promise;
		}

		this.edit = function(data){
			var deff=$q.defer();
			
			var url ='/admin/user/edit_user'

			$http.post(url,data).then(function(result){
				deff.resolve(result.data);
			},function(err){
				deff.reject(err);
			})

			return deff.promise;
		};

		/*this.details = function(data){
			var deff=$q.defer();
			
			var url ='/admin/user/user_detail/'

			$http.post(url,data).then(function(result){
				deff.resolve(result.data);
			},function(err){
				deff.reject(err);
			})

			return deff.promise;
		};*/

	}])
})()