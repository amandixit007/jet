(function(){
	angular.module('controllers')
	.service('agent', ['$q', '$http','baseURL', function($q, $http,baseURL){
		var self=this;
		this.bulkUpload =function(files){
			var deff=$q.defer();
			var fd = new FormData();
           var arr=[];
           for(var key in files)
           	fd.append('imgs', files[key]);
           	//arr.push($scope.aircraft.files[key])
           //fd.append('imgs', arr);
            
           $http.post("/media/upload", fd, {
              transformRequest: angular.identity,
              headers: {'Content-Type': undefined}
           })
        
           .success(function(objS){
           	console.log(objS);
           	deff.resolve(objS)
           })
        
           .error(function(objE){
           	console.log(objE)
           	deff.reject(objE);
           });
			return deff.promise;
		}
		this.startBulkUpload=function(filesPath){
			var deff=$q.defer();
           // $http.post("admin/agent/startBulkUpload",{filesPath:filesPath})
            $http.post("admin/airport/bulkuploadAirport",{filesPath:filesPath})
        
           .success(function(objS){
           	console.log(objS);
           	deff.resolve(objS)
           })
        
           .error(function(objE){
           	console.log(objE)
           	deff.reject(objE);
           });
			return deff.promise;
		}
		this.createUpdateAgent=function(agent){
			var deff=$q.defer();
			var url="/admin/agent/createUpdateAgent";
			$http.post(url,agent)
			.then(function(objS){
				deff.resolve(objS)
			},function(objE){
				console.log("objE---->",objE);
				deff.reject(objE);
			})

			return deff.promise;
		};
		this.getAll=function(){
			var deff=$q.defer();
			//http://13.126.28.231:1105/admin/aircraft/getAllaircraft 
			var url="/admin/agent/viewAllagent";
			$http.get(baseURL+url)
			.then(function(objS){
				deff.resolve(objS.data)
			},function(objE){
				console.log("objE---->",objE);
				deff.reject(objE);
			})

			return deff.promise;
		};
		this.generateAgencyCode=function(city_code){
			var deff=$q.defer();
			var url="/admin/agent/genrateNumber";
			$http.post(url,city_code)
			.then(function(objS){
				deff.resolve(objS.data)
			},function(objE){
				console.log("objE---->",objE);
				deff.reject(objE);
			})

			return deff.promise;
		};
		this.getAgentList=function(){
			var deff=$q.defer();
			//http://13.126.28.231:1105/admin/aircraft/getAllaircraft 
			var url="/admin/agent/viewAllagent";
			$http.get(url)
			.then(function(objS){
				deff.resolve(objS.data)
			},function(objE){
				console.log("objE---->",objE);
				deff.reject(objE);
			})

			return deff.promise;
		};
	}])
})()