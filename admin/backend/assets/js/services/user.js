(function(){
	angular.module('controllers')
	.service('user', ['$q', '$http','baseURL', function($q, $http,baseURL){

		this.login = function(data){
			var deff=$q.defer();

			$http.post(baseURL+"oprator/login",data).then(function(result){
				deff.resolve(result);
			},function(err){
				deff.reject(err);
			})

			return deff.promise;
		};
		
		this.list = function(){
			var deff=$q.defer();
			var url = baseURL+'master/all_user_list'

			$http.get(url).then(function(result){
				console.log(result)
				deff.resolve(result.data);
			},function(err){
				deff.reject(err);
			})

			return deff.promise;
		}

	}])
})()