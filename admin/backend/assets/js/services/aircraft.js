(function(){
	angular.module('controllers')
	.service('aircraft', ['$q', '$http','baseURL', function($q, $http,baseURL){
		var self=this;
		this.uploadFiles=function(files){
			var deff=$q.defer();
			var fd = new FormData();
           var arr=[];
           for(var key in files)
           	fd.append('imgs', files[key]);
           	//arr.push($scope.aircraft.files[key])
           //fd.append('imgs', arr);
            
           $http.post("/media/upload", fd, {
              transformRequest: angular.identity,
              headers: {'Content-Type': undefined}
           })
        
           .success(function(objS){
           	console.log(objS);
           	deff.resolve(objS)
           })
        
           .error(function(objE){
           	console.log(objE)
           	deff.reject(objE);
           });
			return deff.promise;
		}

		this.deleteAircraftImg=function(file){
			var deff=$q.defer();
			var url="/media/remove";
			if(!file.isTemp)
				url="admin/aircraft/removeimage/";
			$http.post(url,file)
			.then(function(objS){
				deff.resolve(objS)
			},function(objE){
				console.log("objE---->",objE);
				deff.reject(objE);
			})

			return deff.promise;
		}
		this.createUpdateAircraft=function(aircraft){
			var deff=$q.defer();
			var url="/admin/aircraft/createUpdateAircraft";
			$http.post(url,aircraft)
			.then(function(objS){
				deff.resolve(objS)
			},function(objE){
				console.log("objE---->",objE);
				deff.reject(objE);
			})

			return deff.promise;
		};
		this.getAll=function(){
			var deff=$q.defer();
			//http://13.126.28.231:1105/admin/aircraft/getAllaircraft 
			var url="aircraft/getAllaircraft";
			$http.get(baseURL+url)
			.then(function(objS){
				deff.resolve(objS.data)
			},function(objE){
				console.log("objE---->",objE);
				deff.reject(objE);
			})

			return deff.promise;
		}
		this.airports=function(){
			var deff=$q.defer();
			//http://13.126.28.231:1105/admin/aircraft/getAllaircraft 
			var url="/libs/airports.json";
			$http.get(url)
			.then(function(objS){
				deff.resolve(objS.data)
			},function(objE){
				console.log("objE---->",objE);
				deff.reject(objE);
			})

			return deff.promise;
		}
		this.getCountries = function(){
			var deff=$q.defer();
			var url="/admin/master/city";
			$http.get(url)
			.then(function(objS){
				deff.resolve(objS.data)
			},function(objE){
				console.log("objE---->",objE);
				deff.reject(objE);
			})

			return deff.promise;
		}
	}])
})()