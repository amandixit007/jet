// Gateway Controller //
var config=require.main.require('./global/config');
var jwt = require('jsonwebtoken');
var user=require.main.require('./db/opratorSchema.js');
var async=require("async");
var aircraft  = require('./db/aircraftSchema.js')


var controller={



     bulkUploadOprator : function(req,res){
       var missingCities=[]
    async.waterfall([function(cb){
         xmlCsvParser.parse("temp/CollegeSheet.xlsx")
        .then(function(objS){
            cb(null,objS["CollegeSheet"])
        },function(objE){
          cb(objE,null)
        })
    },function(xclData,cb){
      console.log("xcalDataaa--------",xclData)
      async.map(xclData,function(x,cbMap){
        var obj={
          oprator_name:x["CollegeName"],
          address:x["COMMUNICATION ADDRESS"]   
        }

        var operator_save = new user(obj)
        operator_save.save(function(err,result){
           err?cbMap(err,null):cbMap(null,result)
        })
        // aircraft.find({jet_name:x["Model"]},function(err,result){
                
        // })
     
      },function(err,result){
        err?cb(err,null):cb(null,result);
      })
    }],function(err,result){
      if(err)
        console.log(err)
      console.log("missingCities---->",missingCities)
        err?res.status(500).send(err):res.send(missingCities);
    })
    




     },




    login:function(req,res){
    	console.log(req.body);
        // if(req.body.username!=config.adminCredential.username){
        //     // res.status(403).send({msg:"username is not matched"});
        //     user.findOne({user_name:req.body.username,password:req.body.password},function(err,result){
        //       console.log("resultt",result)
        //          var _token=jwt.sign({
        //       exp: Math.floor(Date.now() / 1000) + (60 * 60),
        //       data: req.body
        //     }, config.authSecretKey);
        //     res.send({result:result,token:_token})
        //       return ;
        //     })
            
        // }
        // else if(req.body.password!=config.adminCredential.password){
        //     res.status(403).send({msg:"Password is not matched"});
        //     return ;
        // }
        // else{
        //     var _token=jwt.sign({
        //       exp: Math.floor(Date.now() / 1000) + (60 * 60),
        //       data: req.body
        //     }, config.authSecretKey);
        //     res.send({username:req.body.username,token:_token})
        // }
      user.findOne({user_name:req.body.username},function(err,result){
        console.log("result",result)
       if(!result){
        res.status(403).send({msg:"username is not matched"});
       }
       else{
        user.findOne({password:req.body.password},function(err,result1){
        if(!result1){
          res.status(403).send({msg:"password is not matched"});
        }
        else{
           var _token=jwt.sign({
              exp: Math.floor(Date.now() / 1000) + (60 * 60),
              data: req.body
            }, config.authSecretKey);
            res.send({result:result1,token:_token})

        }         



        })
       }




      })
    

    },

    'user_signup': function (req, res) {
        console.log("req" + JSON.stringify(req.body));
        user.findOne({
            email_id: req.body.email_id
              }, function (err, result) {
            console.log("signupresult",result);
            if (err) {
                res.status(500).send({msg:"Server error"});
            } else if (result) {
                res.status(404).send({msg:"Email is already exist"});
            } else {
                var user_save = new user(req.body)
                user_save.save(function (err, result) {
                    if (err) {
                        res.status(500).send({msg:"Server error"});
                    } else {
                        res.status(200).send({
                            response_code: 200,
                            response_message: "Register successfully",
                            result: result

                        })
                        
                    }
                })

             }

         })

     },

     user_list : function(req,res){
      user.find({is_active:"active"},function(err,results){
          if(err){
              res.status(500).send({msg:"Server error"});
          }
          else if(results.length == 0){
              res.status(404).send({msg:"No User Found"});
          }
          else{
               res.status(200).send({
                            response_code: 200,
                            response_message: "All User Found",
                            result: results

                        })
          }
   
      })     
        
    },

    user_detail: function(req,res){
    console.log("request",req.body)
   user.findOne({_id:req.body._id},function(err,result){
   res.status(200).send({
                            response_code: 200,
                            response_message: "detail",
                            result: result

                        })

   })



    },



    'delete_user':function(req,res){
      console.log("delete",req.body);
        user.findByIdAndUpdate({_id:req.body._id},{$set:{is_active:"delete"}},{new:true},function(err,result){
           if(err){
                 res.status(500).send({msg:"Server error"});
            }
            else{
                 res.status(200).send({
                            response_code: 200,
                            response_message: "Delete User successfully",
                            result: result

                        })
            }

        })


    },

    'edit_user':function(req,res){
        console.log("editUserDetail",req.body);
        user.findByIdAndUpdate({_id:req.body._id,is_active:"active"},{$set:req.body},{new:true},function(err,result){
           if(err){
                 res.status(500).send({msg:"Server error"});
            }
            else{
                res.status(200).send({
                            response_code: 200,
                            response_message: "UserInfo Update successfully",
                            result: result

                        })
            }

        })
    }
}

module.exports = controller;


