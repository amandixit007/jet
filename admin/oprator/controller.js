// Gateway Controller //
var config=require.main.require('./global/config');
var jwt = require('jsonwebtoken');
var mongoose = require('mongoose');
var opertaor=require.main.require('./db/opratorSchema.js');
var aircraft = require.main.require('./db/aircraftSchema.js')
var country=require.main.require('./db/countrySchema.js');
var opertaorJet = require.main.require('./db/opretorAircraft.js')
var aircraftPrice = require.main.require('./db/priceSchema.js')
var async=require("async");
var xmlCsvParser=require.main.require('./global/xmlCsvParser');


var controller={


        bulkUploadOprator: function(req,res,next){
            console.log("enter")
        var missingCities=[]
        async.waterfall([function(cb){
           xmlCsvParser.parse("temp/1510057623660.xlsx")
               //   xmlCsvParser.parse(req.body.filesPath)
         
          .then(function(objS){
              cb(null,objS["Operator "])
          },function(objE){
            cb(objE,null)
          })
      },
      function(xclData,cb){
          var duplicate = []
         //  console.log("xclData",xclData)
           async.map(xclData,function(x,cbMap){
        var query=[{
          $unwind:"$states"
        },{
          $unwind:"$states.cities"
        },{
                    $match:{
                        "states.cities.cityName":x["CITY"].toUpperCase()
                    }
                }];
        country.aggregate(query)
        .exec(function(err,result){
         //   console.log("result1",result)
          if(err)
            cb(err,null)
    
//            async.map(xclData,function(x,cbMap){
//              var getcityState=result.filter(y=>y.states.cities.cityName.toUpperCase()==x["CITY"]);
//            console.log("getcityState--------",getcityState)
//              var stateId=0,cityId=0;
              if(result.length>0){
                x.cityId=result[0].states.cities._id
                x.stateId=result[0].states._id
                var query={
                    user_name:x["OPERATOR'S NAME"]
                };
                var value={
                    city:x.cityId,
                    state:x.stateId,
                    user_name:x["OPERATOR'S NAME"],
                    code:x["Code"],
                    address:x["COMMUNICATION ADDRESS"],
                   fax:x["FAX"],
               //     model:x["Model"],
                  //  aircraftId:[],
                    email:x["EMAIL"],
                    AOP_NO:x["AOP NO"],
                    valid_upto:x["Valid Upto"],
                   type:"oprator"
                };
                opertaor.findOneAndUpdate(query,value,{upsert:true,new:true})
                .exec(function(err,savedAirport){
                    opertaor.findOneAndUpdate({user_name:savedAirport.user_name},{$addToSet:{model:x["Model"]}},{new:true},function(err,newResult){
                         err?cbMap(err,null):cbMap(null,newResult);
                    })
                   
                })
        }
            else{
                       cbMap(null,{})
                   }
            })              
            },function(err,result){
                 err?cb(err):cb(null,result)

            })
    
      },function(data,cb){
           async.map(data,function(x,cbMap){
            //   console.log("xxxxxxxxx",x)
               aircraft.findOne({$and:[{"jet_name":{$in:x.model}},{jet_name:{$ne:null}}]},function(err,result2){
                   //console.log("result2",result2)
                   if(result2!=null){
                    aircraftPrice.findOne({aircraft_id:mongoose.Types.ObjectId(result2._id)},function(err,result3){
                      //  console.log("result3",result3)
                       if(err)
                           cbMap(err)
                       else if(result3!=null){
                           var query1 = {
                               jet_name:result2.jet_name
                           };
                           
                         var value1={
                               jet_name:result2.jet_name,
                            //   opratorId:x._id,
                               image_gallary:result2.image_gallary,
                               bases:result2.bases,
                               baggage:result2.baggage,
                               weight:result2.weight,
                               speed:result2.speed,
                               minimum_distance:result2.minimum_distance,
                               pilot:result2.pilot,
                          //     AOP_NO:x.AOP_NO,
                           //     valid_upto:x.valid_upto,
                                price:result3.price

                               
                           }
                         opertaorJet.findOneAndUpdate(query1,value1,{upsert:true}).exec(function(err,result33){
                               console.log("result33",result33)
                               cbMap(null,result33);
                         })
//                         var restsave = new opertaorJet(value1)
//                         restsave.save(function(err,result33){
//                              cbMap(null,result33);
//                         })
                         
                       
                   }
                    })
                   }
                   else{
                       cbMap(null,{})
                   }
     
               })
           },function(err,result){
               //console.log("secound result",result)
                err?cb(err,null):cb(null,result);
              
          }
                    )
      }],function(err,result){
        if(err)
          console.log(err)
        console.log("missingCities---->",missingCities)
          err?res.status(500).send(err):res.send(missingCities);
      })
            

    },
    
    
    
    
    bulkUploadAgent : function(req,res,next){
      console.log("enter")
              var missingCities=[]
        async.waterfall([function(cb){
           xmlCsvParser.parse("temp/1510919866735.xlsx")
               //   xmlCsvParser.parse(req.body.filesPath)
         
          .then(function(objS){
              cb(null,objS["IATA & CORP"])
          },function(objE){
            cb(objE,null)
          })
      },
      function(xclData,cb){
          var duplicate = []
           async.map(xclData,function(x,cbMap){
        var query=[{
          $unwind:"$states"
        },{
          $unwind:"$states.cities"
        }];
        country.aggregate(query)
        .exec(function(err,result){
        if(err)
            cb(err,null)
              var getcityState=result.filter(y=>y.states.cities.cityName.toUpperCase()==x["CITY"]);
              var stateId=0,cityId=0;
                 x.cityId=result[0].states.cities._id;
                x.stateId=result[0].states._id;
                var query={
                    user_name:x["CO-NAME"]
                };
                var value={
                    city:x.cityId,
                    state:x.stateId,
                    user_name:x["CO-NAME"],
                    code:x["Code"],
                    address1:x["ADD1"],
                    address2:x["ADD2"],
                    fax:x["FAX"],
                    phone_number:x["PHONE"],
                    pincode:x["PIN"],
                    email:x["EMAIL"],
                    website:x["Website"],
                    keyPerson:[{
                    name:x["Key Person"],
                    designation:x["Key Person Designation"],
                        }],
                   type:"agent"
                };
                opertaor.findOneAndUpdate(query,value,{upsert:true,new:true})
                .exec(function(err,savedAirport){
                    err?cbMap(err,null):cbMap(null,savedAirport);
                })
            })              
            },function(err,result){
                 err?cb(err):cb(null,result)

            })
    
      }],function(err,result){
        if(err)
          console.log(err)
        console.log("missingCities---->",missingCities)
          err?res.status(500).send(err):res.send(missingCities);
      })
        
        
    },



    login:function(req,res){
    	console.log(req.body);
        // if(req.body.username!=config.adminCredential.username){
        //     // res.status(403).send({msg:"username is not matched"});
        //     user.findOne({user_name:req.body.username,password:req.body.password},function(err,result){
        //       console.log("resultt",result)
        //          var _token=jwt.sign({
        //       exp: Math.floor(Date.now() / 1000) + (60 * 60),
        //       data: req.body
        //     }, config.authSecretKey);
        //     res.send({result:result,token:_token})
        //       return ;
        //     })
            
        // }
        // else if(req.body.password!=config.adminCredential.password){
        //     res.status(403).send({msg:"Password is not matched"});
        //     return ;
        // }
        // else{
        //     var _token=jwt.sign({
        //       exp: Math.floor(Date.now() / 1000) + (60 * 60),
        //       data: req.body
        //     }, config.authSecretKey);
        //     res.send({username:req.body.username,token:_token})
        // }
      opertaor.findOne({user_name:req.body.username},function(err,result){
        console.log("result",result)
       if(!result){
        res.status(403).send({msg:"username is not matched"});
       }
       else{
        opertaor.findOne({password:req.body.password},function(err,result1){
        if(!result1){
          res.status(403).send({msg:"password is not matched"});
        }
        else{
           var _token=jwt.sign({
              exp: Math.floor(Date.now() / 1000) + (60 * 60),
              data: req.body
            }, config.authSecretKey);
            res.send({result:result1,token:_token})

        }         



        })
       }




      })
    

    },

    'user_signup': function (req, res) {
        console.log("req" + JSON.stringify(req.body));
        user.findOne({
            email_id: req.body.email_id
              }, function (err, result) {
            console.log("signupresult",result);
            if (err) {
                res.status(500).send({msg:"Server error"});
            } else if (result) {
                res.status(404).send({msg:"Email is already exist"});
            } else {
                var user_save = new user(req.body)
                user_save.save(function (err, result) {
                    if (err) {
                        res.status(500).send({msg:"Server error"});
                    } else {
                        res.status(200).send({
                            response_code: 200,
                            response_message: "Register successfully",
                            result: result

                        })
                        
                    }
                })

             }

         })

     },

     user_list : function(req,res){
      user.find({is_active:"active"},function(err,results){
          if(err){
              res.status(500).send({msg:"Server error"});
          }
          else if(results.length == 0){
              res.status(404).send({msg:"No User Found"});
          }
          else{
               res.status(200).send({
                            response_code: 200,
                            response_message: "All User Found",
                            result: results

                        })
          }
   
      })     
        
    },

    user_detail: function(req,res){
    console.log("request",req.body)
   user.findOne({_id:req.body._id},function(err,result){
   res.status(200).send({
                            response_code: 200,
                            response_message: "detail",
                            result: result

                        })

   })



    },



    'delete_user':function(req,res){
      console.log("delete",req.body);
        user.findByIdAndUpdate({_id:req.body._id},{$set:{is_active:"delete"}},{new:true},function(err,result){
           if(err){
                 res.status(500).send({msg:"Server error"});
            }
            else{
                 res.status(200).send({
                            response_code: 200,
                            response_message: "Delete User successfully",
                            result: result

                        })
            }

        })


    },

    'edit_user':function(req,res){
        console.log("editUserDetail",req.body);
        user.findByIdAndUpdate({_id:req.body._id,is_active:"active"},{$set:req.body},{new:true},function(err,result){
           if(err){
                 res.status(500).send({msg:"Server error"});
            }
            else{
                res.status(200).send({
                            response_code: 200,
                            response_message: "UserInfo Update successfully",
                            result: result

                        })
            }

        })
    }
}

module.exports = controller;


