// admin routes//

var express = require('express'); 
var path=require('path');   
var router = express.Router();  
var authorisation = require('../authorisation');
var controller = require('./controller');

router.post('/login',controller.login);
router.post('/user_signup',controller.user_signup);
router.get('/user_list',controller.user_list);
router.post('/delete_user', controller.delete_user);
router.post('/edit_user', controller.edit_user);
router.post('/user_detail', controller.user_detail);

 router.get('/bulkUploadOprator',controller.bulkUploadOprator);
 router.get('/bulkUploadAgent',controller.bulkUploadAgent);

// router.get('/',authorisation.);  

module.exports = router;  