var config=require.main.require('./global/config');
var aircraft=require.main.require('./db/aircraftSchema.js');
var country=require.main.require('./db/countrySchema.js');
var airportSchema=require.main.require('./db/airportSchema.js');
var airportDistanceSchema=require.main.require('./db/airportDistanceSchema.js');
var xmlCsvParser=require.main.require('./global/xmlCsvParser');
var mongoose = require('mongoose');
var https = require('https');
var async = require('async');
// var RegExp=require("RegExp")


//{"file":"temp/1507115471892.xlsx","isTemp":true}

var airport = {
    bulkuploadHandlingCharges:function(req,res,next){
        // req.body={"filesPath":"temp/1507120056556.xlsx"}
       //  req.body={"filesPath":"temp/1509540514170.xlsx"}
       req.body={"filesPath":"temp/1509963041033.xlsx"}

        
        async.waterfall([function(cb){
            xmlCsvParser.parse(req.body.filesPath)
            .then(function(objS){
              cb(null,objS["Ground Handling"])
            },function(objE){
              cb(objE,null)
            })
        },function(xclData,cb){
            console.log("xclData-->",xclData[0])
            async.map(xclData,function(x,cbMap){
                var update={
                    $set:{
                        groundhandling:{
                            W05:x["W05"]||0,
                            W510:x["W510"]||0,
                            W1015:x["W1015"]||0,
                            W1520:x["W1520"]||0,
                            W2030:x["W2030"]||0,
                            W3040:x["W3040"]||0,
                            W4050:x["W4050"]||0,
                            W5070:x["W5070"]||0,
                            W70M:x["W70M"]||0
                        }
                    }
                };
                // console.log("update--->",update)
                airportSchema.update({icao:x["ICAO"]},update)
                .exec(function(err,result){
                    err?cbMap(err,null):cbMap(null,result);
                })
            },function(err,result){
                err?cb(err,null):cb(null,result);
            })
        }],function(err,result){
            err?res.status(500).send(err):res.send(result)
        })
    },
    bulkuploadAirport:function(req,res,next){
        req.body={"filesPath":"temp/1511352605157.xlsx"};
      // req.body={"filesPath":"temp/1509450087910.xlsx"};
       
        async.waterfall([function(cb){
            xmlCsvParser.parse(req.body.filesPath)
            .then(function(objS){
              cb(null,objS["Airport Data"])
            },function(objE){
              cb(objE,null)
            })
        },function(data,cb){
           var cities=[];
           console.log("data",data)
           async.map(data,function(x,cbMap){
            var query=[
                {
                    $unwind:"$states"
                },
                {
                    $unwind:"$states.cities"
                },
                {
                    $match:{
                        "states.cities.cityName":x["City Served"].toUpperCase()
                    }
                }
            ];
            country.aggregate(query)
            .exec(function(err,result){
                console.log("result--->",result)
                if(err)
                    cbMap(err,null)
                x.cityId=result[0].states.cities._id
                x.stateId=result[0].states._id

                 var query={
                    airport:x["Airport Name"]
                };
                var value={
                    city:x.cityId,
                    country:result[0]._id,
                    state:x.stateId,
                    airport:x["Airport Name"],
                    icao:x["ICAO"],
                    iata:x["IATA"],
                    category:x["Category"],
                    role:x["Role"],
                    runway:x["Runway (FT.)"],
                    createdBy:"admin"
                };
                // cbMap(value,null)
                airportSchema.findOneAndUpdate(query,value,{upsert:true,new:true})
                .exec(function(err,savedAirport){
                    err?cbMap(err,null):cbMap(null,savedAirport);
                })
                //cbMap(null,x)

            })
           },function(err,result){
                err?cb(err):cb(null,result)
           })
           // for(var i=0;i<data.length;i++){
           //  var _states=data[i]["STATE"].toUpperCase();
           //  var _city=data[i]["City Served"].toUpperCase();
           //  if(_states!=null && (cities.findIndex(x=>x.cityName==_city)==-1)){
           //      cities.push({stateName:_states,cityName:_city});
           //  }
           // }
        },function(data,cb){
            cb(null,data);
        }],function(err,result){
            err?res.status(err):res.send(result);
        })
    },
    bulkuploadAirport1:function(req,res,next){
  //console.log(req.body)
  req.body={"filesPath":"temp/1507030082840.xlsx"}
  async.waterfall([function(cb){
    xmlCsvParser.parse(req.body.filesPath)
    .then(function(objS){
      cb(null,objS)
    },function(objE){
      cb(objE,null)
    })
  },function(data,cb){
    var query=[{
      $unwind:"$states"
    },{
      $match:{$and:[{
        "states.stateName":{
          $exists:true
        }
      },{
        "countryName":"India"
      }]}
    }];
    country.aggregate(query).exec(function(err,result){
       err?cb(err,null):cb(null,data,result);
     });
  },function(airports,data,cb){
    // console.log("airports---------------------->")
    // console.log(airports)
    // cb(null,data);
    async.map(airports["India"],function(x,cbMap){
        if(!x["STATE"])
            cbMap(null,x);
        var airport=x;
        // var query={

        // };
        // var values={

        // }
        // country.update(query,)
       //console.log("states------>",x["STATE"].toUpperCase())
      
        var _airport=data.find(y=>{
                return x["STATE"].toUpperCase()==y.states.stateName.toUpperCase();
        });
         //console.log("_airport----->",_airport)
        if(!_airport){
            //console.log("airport if--->")
            airport.stateId=0;
            airport.cityId=0;
        }
        else{
            //console.log("airport else--->")
            // console.log("_airport--->",_airport)
            airport.stateId=_airport._id;
            if(!_airport.states.cities){
                //console.log("cities if--->")

                airport.cityId=0;
            }
            else{
                //console.log("cities else--->")
                if(!x["City Served"]){
                    airport.cityId=0;
                    airport["City Served"]="default";
                }
                else{
                   var _city=_airport.states.cities.find(z=>{
                        if(z.cityName && x["City Served"])
                            return z.cityName.toUpperCase()==x["City Served"].toUpperCase();
                        else 
                            return false;
                    });
                  // console.log("_city-->",_city);
                    if(!_city){
                        airport.cityId=0;
                    }
                    else{
                        airport.cityId=_city._id;
                    } 
                }
            }
            
        }
        cbMap(null,airport)
    },function(err,result){
        err?cb(err,null):cb(null,result);
    })
    
  },function(result,cb){
    var _newStates=[];
    result.forEach(x=>{
        //console.log("x--->",x);
        if(x.stateId==0 && _newStates.findIndex(y=>y.stateName==x["STATE"])==-1)
            _newStates.push({stateName:x["STATE"],createdBy:"admin"})
    })
   // console.log("_newStates---->",_newStates)
    cb(null,_newStates,result);
  },function(newSates,result,cb){
    //console.log("newSates--->",newSates)
    country.update({
        countryName:"India"
    },{
        $push:{
            states:{
                $each:newSates
            }
        }
    }).exec(function(err,addStates){
        if(err)
            console.log("err---->",err)
        else{
            //console.log("addStates--->",addStates)
            cb(null,result);
        }
        
    })
    // async.map(newSates,function(x,cbmap){
    //     country.findOneAndUpdate({countryName:"India"},{

    //     },{new:true}).exec(function(err,result){
    //         if(err)
    //             cbmap(err,null);
    //         x.stateId=
    //         cbmap(null,)
    //     })
    // },function(err,result){

    // })
    
  },function(result,cb){
    async.map(result.filter(x=>x["cityId"]==0 && x["City Served"]!=="default"),function(x,cbmap){
        console.log('x["City Served"]--->',x)
        country.update({
            countryName:"India",
            "states.stateName":x["STATE"]
        },{
            $addToSet:{
                "states.$.cities":{
                    cityName:x["City Served"]
                }
            }
        },{new:true,multi:true}).exec(function(err,addStates){
            if(err)
                cbmap(err,null);
            else{
                //console.log("addStates--->",addStates)
                cbmap(null,result);
            }
            
        })
    },function(err,dataResponse){
        err?cb(err,null):cb(null,result)
    })
  },function(result,cb){
    var query=[{
        $match:{
            countryName:"India"
        }
    },{
        $unwind:"$states"
    },{
        $unwind:"$states.cities"
    }];
    country.aggregate(query)
    .exec(function(err,rows){
        if(err)
            cb(err,null)
        else{
            console.log("rows--->",rows);
            var states={},cities={},len=rows.length;
            for(var i=0;i<len;i++){
                states[rows[i].states.stateName]=rows[i].states._id;
                cities[rows[i].states.cities.cityName]=rows[i].states.cities._id;
            }
            cb(null,rows[0]._id,states,cities,result)
            // if(rows.length==0){
            //     country.find({countryName:"India"})
            //     .exec(function(err,rows1){
            //         cb(null,rows1[0]._id,[],[],result)
            //     })
            // }
            // else{
            //     console.log("rows--->",rows);
            //     var states={},cities={},len=rows.length;
            //     for(var i=0;i<len;i++){
            //         states[rows[i].states.stateName]=rows[i].states._id;
            //         cities[rows[i].states.cities.cityName]=rows[i].states.cities._id;
            //     }
            //     cb(null,rows[0]._id,states,cities,result)
            // }
        }
    })
  },function(countryId,states,cities,parseData,cb){
    // console.log("result--->",parseData)
    async.map(parseData,function(x,cbMap){
        var query={
            airport:x["Airport Name"]
        };
        var value={
            city:cities[x["City Served"]],
            country:countryId,
            state:states[x["STATE"]],
            airport:x["Airport Name"],
            icao:x["ICAO"],
            iata:x["IATA"],
            category:x["Category"],
            role:x["Role"],
            createdBy:"admin"
        };
        // cbMap(value,null)
        airportSchema.findOneAndUpdate(query,value,{upsert:true,new:true})
        .exec(function(err,savedAirport){
            err?cbMap(err,null):cbMap(null,savedAirport);
        })
    },function(err,result){
        err?cb(err,null):cb(err,result)
    })
  }],function(err,result){
    err?res.status(500).send(err):res.send(result)
  })
  
  },
  bulkuploadAirportDistance:function(req,res,next){
    //req.body.filesPath="temp/1506513772687.xlsx";
    console.log("bulkuploadAirportDistance----->")
    var missingCities=[]
    async.waterfall([function(cb){
        // xmlCsvParser.parse("temp/1507030251404.xlsx")
         // xmlCsvParser.parse("temp/1508932934117.xlsx")
           xmlCsvParser.parse("temp/15095132970971.xlsx")
         
        .then(function(objS){
            cb(null,objS["City Pairs"])
        },function(objE){
          cb(objE,null)
        })
    },function(xclData,cb){
    	console.log(xclData[0])
            // airportSchema.find({airport:{$regex:x["From ICAO"],$option:"i"}},function(err,_airport){
            	//new RegExp("^" + thename.toLowerCase(), "i")

          
        async.map(xclData,function(x,cbMap){
            // airportSchema.find({airport:{$regex:x["From City"],$option:"i"}},function(err,_airport){
            	// console.log('x["From City"]--->',x["From City"])
            	// console.log('x["To City"]--->',x["To City"])
            // airportSchema.find({airport:new RegExp(x["From City"],'i')},function(err,_airportFrom){
            airportSchema.find({icao:x["From ICAO"]},function(err,_airportFrom){
                if(err){
                	cbMap(err,null)
                }
                else{
                	 console.log("_airport--->",_airportFrom)
                	if(_airportFrom.length>0)
                		x.fromAirport=_airportFrom[0];
                	else{
                		missingCities.push(x["From City"]);
                	}
                	// airportSchema.find({airport:x["To City"]},function(err,_airport){
                	// airportSchema.find({airport:{$regex:x["To City"],$option:"i"}},function(err,_airport){
            		airportSchema.find({icao:x["To ICAO"]},function(err,_airportTo){
                    	if(err){
                    		cbMap(err,null)
                    	}
                    	else{
                    		if(_airportTo.length>0)
                    			x.toAirport=_airportTo[0];
                    		else{
                    			missingCities.push(x["To City"]);
                    		}
                    		cbMap(null,x)
                    	}
                    });
                }
            });
        },function(err,result){
            err?cb(err,null):cb(null,result);
        })
    },function(xclData,cb){
    	console.log("xclData--->",xclData.length);
    	async.map(xclData.filter(x=>x.fromAirport && x.toAirport),function(x,cbMap){
    		console.log("x--->",x)
    		var query={$and:[{
    			fromAirport:x.fromAirport._id,
    			toAirport:x.toAirport._id
    		}]}
    		var obj={
    			fromAirport:x.fromAirport._id,
    			toAirport:x.toAirport._id,
    			distance:parseInt(x.Distance),
    			createdBy:"admin"
    		}
    		//var objSaveDistance=new airportSchema(obj)
    		airportDistanceSchema.findOneAndUpdate(query,obj,{new:true,upsert:true},function(err,result){
    			if(err)
    				throw err;
    			console.log(result)
    			err?cbMap(err,null):cbMap(null,result);
    		});
    	},function(err,result){
    		err?cb(err,null):cb(null,xclData);
    	})
    }],function(err,result){
    	if(err)
    		console.log(err)
    	console.log("missingCities---->",missingCities)
        err?res.status(500).send(err):res.send(missingCities);
    })
  },
  groundhandlingCharge:function(x,cb){
    console.log("xxxx",x)
 // 	airportSchema.findOne({_id: mongoose.Types.ObjectId(x)},{"groundhandling":1},function(err,result){
//    airportSchema.findOne({_id:mongoose.Types.ObjectId(x)},{"groundhandling":1},function(err,result){
         airportSchema.findOne({_id:mongoose.Types.ObjectId(x)},function(err,result){
		cb(null,result);
	})
  },
  airportdistance:function(x,cb){
    console.log("x--------->",x);
  	airportDistanceSchema.findOne({fromAirport: x.origin,toAirport:x.destination},function(err,result){
        if(!result){
            cb(null,{fromAirport:x.origin,toAirport:x.destination,distance:0,duration:0,flightDuration:x.flightDuration});
        }
        else{
		  cb(null,{fromAirport:x.origin,toAirport:x.destination,distance:result.distance,duration:0,flightDuration:x.flightDuration});
        }
	})
  },

  allairportDistance : function(req,res){
    // async.parallel({
    //     count:function(cb){
    //         airportDistanceSchema.count().exec(function(err,result){
    //             err?cb(err):cb(null,result);
    //         })
    //     },
    //     records:function(cb){
    //         airportDistanceSchema.find({}).limit(req.params.limit).skip(req.params.offset).exec(function(err,result){
    //             console.log("result",result)
    //              err?cb(err):cb(null,result);
    //         });
    //     }
    // },function(err,result){
    //     err?res.status(500).send(err):res.send(result);
    // })
    var query  = {};
      var options = {
                page:   req.params.page_number, 
                limit:    10
            };
    airportDistanceSchema.paginate(query,options,function(err,result){
        console.log("result",result)
        if(err){
            res.send({response_code:500,response_message:"server err"})
        }
        else{
            var pagination ={};
                    pagination.total = result.total;
                    pagination.limit= result.limit;
                    pagination.page= result.page;
                    pagination.pages= result.pages;
                   res.send({result:result.docs ,pagination:pagination})
        }


    })
  },

  addUpdateAirport : function(req,res){
  console.log("req",req.body)
  if(!req.body._id){
airportSchema.findOne({airport:req.body.airport},function(err,result){
    console.log("result",result)
      if(err) {  return res.send({status:500,message:"something went wrong"}) }
      else if(!result){
        var airport_save = new airportSchema(req.body)
        airport_save.save(function(err,result){
              err?res.status(500).send(err):res.send(result);
        })

      }
      else{
        return res.send({status:404,message:"Airport Name must be unique"}) 
      }

})
}

else{
     airportSchema.findByIdAndUpdate(req.body._id, {$set:req.body}, function(error,result) {
            if(error) { throw error; }
            error?res.status(500).send(error):res.send(result)
                //...
            })


}

  },

addUpdateAirportDistance:function(req,res){
 console.log("req",req.body)
  if(!req.body._id){
   var airportdistance_save = new airportDistanceSchema(req.body)
   airportdistance_save.save(function(err,result){
  err?res.status(500).send(err):res.send(result)     

   })

  }
  else{
     airportDistanceSchema.findByIdAndUpdate(req.body._id, {$set:req.body}, function(error,result) {
            if(error) { throw error; }
            error?res.status(500).send(error):res.send(result)
                //...
            })

  }

},
addNewCitieStates:function(req,res){
     req.body={"filesPath":"temp/1509450087910.xlsx"};
       //db.countries.find({$and:[{countryName:"India"},{states:{$elemMatch:{stateName:"DELHIs"}}}]}).pretty()
//db.countries.find({$and:[{countryName:"India"},{states:{$elemMatch:{$and:[{stateName:"DELHI"},{cities:{$elemMatch:{cityName:"NEW DELHI"}}}]}}}]}).pretty()
        async.waterfall([function(cb){
            xmlCsvParser.parse(req.body.filesPath)
            .then(function(objS){
              cb(null,objS["Airport Data"])
            },function(objE){
              cb(objE,null)
            })
        },function(data,cb){
            var states=[];
            for(var i=0;i<data.length;i++){
                console.log("i--->",i);
                console.log("data--->",data[i]);
                var _indx=states.findIndex(x=>x==data[i]["STATE"].toUpperCase())
                console.log("states",data[i].STATE)
                if(_indx==-1)
                    states.push(data[i]["STATE"].toUpperCase());
            }
            async.map(states,function(x,cbMap){
                country.find({$and:[{countryName:"India"},{states:{$elemMatch:{stateName:x}}}]},function(err,result){
                    if(err)
                        res.status(500).send(err);
                    else{
                        if(result.length==0){
                            console.log("new state found--->",x);
                            country.update({"countryName":"India"},{"$push":{"states":{"stateName":x}}})
                            .exec(function(err,result){
                                console.log("err state---->",err);
                                err?cbMap(err):cbMap(null,result);
                            })
                        }
                    }
                })
            },function(err,result){
                err?cb(err):cb(null,data)
            })
        },function(data,cb){
            var cities=[];
            for(var i=0;i<data.length;i++){
                var _indx=cities.findIndex(x=>x.cityName==data[i]["City Served"].toUpperCase())
                if(_indx==-1)
                    cities.push({cityName:data[i]["City Served"].toUpperCase(),stateName:data[i]["STATE"].toUpperCase()});
            }
            async.map(cities,function(x,cbMap){
                country.find({$and:[{countryName:"India"},{states:{$elemMatch:{$and:[{stateName:x.stateName},{cities:{$elemMatch:{cityName:x.cityName}}}]}}}]},function(err,result){
                    if(err)
                        res.status(500).send(err);
                    else{
                        if(result.length==0){
                            console.log("new state found--->",x);
                            country.update({$and:[{"countryName":"India"},{states:{$elemMatch:{stateName:x.stateName}}}]},{"$push":{"states.$.cities":{"cityName":x.cityName}}})
                            .exec(function(err,result){
                                console.log(" city err--->",err)
                                err?cbMap(err):cbMap(null,result);
                            })
                        }
                    }
                })
            },function(err,result){
                err?cb(err):cb(null,data)
            })
        }],function(err,result){
            err?res.status.send(err):res.send(result);
        })
}




}

module.exports = airport;


var addStatesCities=function(inputs,outputs,cb){
    if(inputs.length==0)
        cb(outputs)
    else{
        //read first element of inputs.
        var obj=inputs[0];
        
        async.waterfall([function(cb){
            country.findOneAndUpdate({countryName:"India",})
        },function(){}],function(err,result){

        })
       
        //findandupdate for state.
        //finandupadte for city.
        // add cityId and stateId to inputs first element
        //add obj to output
        // outputs.push(obj);
        // inputs.pop();
        // addStatesCities(inputs,outputs,cb)
    }
}

//var query=[{
//    $match:{
//        $and:[{
//            $or:[{minimum_distance:{$gt:5000}},{minimum_distance:{$lte:3000}}]
//        },{
//            aircraft_max_range:{$gte:1500}
//        }]
//    }
//},{
//    $addFields:{
//        "hasBase":{
//            $gt:[{
//                $size:{
//                $filter:{
//                    input:"$bases",
//                    as:"x",
//                    cond:{
//                        $and:[{
//                            $eq:["$$x.base_city",ObjectId("5a1569ab9bbcbe86e2a32d92")]
//                        },{
//                            $gt:["$$x.base_price",0]
//                        }]
//                    }
//                }
//            }
//            },0]
//            
//        },
//        activeBases:{
//           $filter:{
//                input:"$bases",
//                as:"x",
//                cond:{
//                   $gt:["$$x.base_price",0]
//                }
//            }
//        },
//        baseDistance:baseDistance,
//        reverseBaseDistance:reverseBaseDistance
//    }
//},{
//    $addFields:{
//        positioning:{
//            $reduce:{
//                input:{
//                    $map:{
//                        input:"$activeBases",
//                        as:"x",
//                        in:{
//                            price:"$$x.base_price",
//                            obj:{
//                                $arrayElemAt:["$baseDistance",{
//                                $reduce:{
//                                    input:"$baseDistance",
//                                    initialValue:-1,
//                                    in:{
//                                        $cond:{
//                                            if:{$eq:["$$x.base_city","$$this.fromAirport"]},
//                                            then:"$$this.indx",
//                                            else:"$$value"
//                                        }
//                                    }
//                                }
//                            }]
//                            }
//                            
//                        }
//                    }
//                },
//                initialValue:{fromAirport:"",toAirport:"",distance:10000000000,price:0},
//                in:{
//                    $cond:{
//                        if:{$eq:["$$value.distance",10000000000]},
//                        then:{fromAirport:"$$this.obj.fromAirport",toAirport:"$$this.obj.toAirport",distance:"$$this.obj.distance",price:"$$this.price"},
//                        else:{
//                            $cond:{
//                                if:{
//                                    $lt:["$$value.distance","$$this.distance"]
//                                },
//                                then:{fromAirport:"$$value.fromAirport",toAirport:"$$value.toAirport",distance:"$$value.distance",price:"$$value.price"},
//                                else:{fromAirport:"$$this.obj.fromAirport",toAirport:"$$this.obj.toAirport",distance:"$$this.obj.distance",price:"$$this.obj.price"}
//                            }
//                        }
//                    }
//                }
//            }
//        }
//    }
//},{
//    $addFields:{
//         repositioning:{
//            $arrayElemAt:[
//                {
//                    $filter:{
//                        input:"$reverseBaseDistance",
//                        as:"x",
//                        cond:{
//                            $and:[{$eq:["$$x.toAirport","$positioning.fromAirport"]},{$eq:["$$x.fromAirport","$positioning.toAirport"]}]
//                        }
//                    }
//                },0]
//        }
//    }
//},{
//    $addFields:{
//        positioning:{
//            "fromAirport" : "$positioning.fromAirport",
//            "toAirport" : "$positioning.toAirport",
//            "distance" : "$positioning.distance",
//            "price" : "$positioning.price",
//            duration:{
//                $add:[_planeStandByTime,{$multiply:[{$divide:["$positioning.distance","$$ROOT.speed"]},60]}]
//            },
//            cost:{
//                $divide:[{
//                    $multiply:["$positioning.price",{$add:[_planeStandByTime,{$multiply:[{$divide:["$positioning.distance","$$ROOT.speed"]},60]}]}]
//                },60]
//            }
//        },
//        repositioning:{
//            fromAirport:"$repositioning.fromAirport",
//            toAirport:"$repositioning.toAirport",
//            distance:"$repositioning.distance",
//            price:"$positioning.price",
//            duration:{
//                $add:[_planeStandByTime,{$multiply:[{$divide:["$repositioning.distance","$$ROOT.speed"]},60]}]
//            },
//            cost:{
//                $divide:[{
//                    $multiply:["$positioning.price",{$add:[_planeStandByTime,{$multiply:[{$divide:["$repositioning.distance","$$ROOT.speed"]},60]}]}]
//                },60]
//            }
//        }
//    }
//},{
//    $project: {
//        documents:"$$ROOT",
//        airports:airports,
//        price:"$positioning.price",
//          duration: {
//            $map:{
//              input: roots,
//              as: "x",
//              in: { 
//                "data":"$$x",
//                "duration":{
//                  $add:[_planeStandByTime,{$multiply:[{$divide:["$$x.distance","$$ROOT.speed"]},60]}]
//                },
//                "crewCharges":{
//                    $cond:{
//                        if:{$gt:[{$subtract:["$$x.flightDuration",{$add:[_planeStandByTime,{$multiply:[{$divide:["$$x.distance","$$ROOT.speed"]},60]}]}]},4*60]},
//                            then:{$multiply:["$$ROOT.crew_charges",{$sum:["$$ROOT.pilot","$$ROOT.crew_members"]}]},
//                        else:0
//                    }
//                }
//              }
//            }
//          }
//        }
//},{
//              $project:{
//                documents:"$documents",
//                price:"$price",
//                duration:"$duration",
//                calculatedDuration:{
//                  $cond:{
//                    if:{$gt:[minimumDuration,{
//                      $reduce:{
//                        input:"$duration",
//                        initialValue:0,
//                        in:{
//                          $add:["$$value","$$this.duration"]
//                        }
//                      }
//                    }]},
//                    then:minimumDuration,
//                    else:{
//                      $reduce:{
//                        input:"$duration",
//                        initialValue:0,
//                        in:{
//                          $add:["$$value","$$this.duration"]
//                        }
//                      }
//                    }
//                  }
//                },
//                totalCrewCharge:{
//                  $sum:[{
//                    $reduce:{
//                      input:"$duration",
//                      initialValue:0,
//                      in:{
//                        $add:["$$value","$$this.crewCharges"]
//                      }
//                    }
//                  },{$multiply:[15000,extraHalt,{$sum:["$documents.pilot","$documents.crew_members"]}]}]
//                }
//                	,
//                groundHandlingCharges:{
//                  $reduce:{
//                    input:"$airports",
//                    initialValue:0,
//                    in:{$add:["$$value", {
//                      $cond:{
//                        
//
//                          if:{
//                            $and:[{$gte:["$documents.weight",0]},{$lte:["$documents.weight",5000]}]
//                          },
//                            then:'$$this.groundhandling.W05',
//                          else:{
//                            $cond:{
//                              if:{
//                                $and:[{$gt:["$documents.weight",5000]},{$lte:["$documents.weight",10000]}]
//                              },
//                                then:'$$this.groundhandling.W510',
//                              else:{
//                                $cond:{
//
//                                  if:{
//                                    $and:[{$gt:["$documents.weight",10000]},{$lte:["$documents.weight",15000]}]
//                                  },
//                                    then:'$$this.groundhandling.W1015',
//                                    else:{
//                                      $cond:{
//                                        if:{
//                                          $and:[{$gt:["$documents.weight",15000]},{$lte:["$documents.weight",20000]}]
//                                        },
//                                        then:'$$this.groundhandling.W1520',
//                                        else:{
//                                          $cond:{
//                                            if:{
//                                              $and:[{$gt:["$documents.weight",20000]},{$lte:["$documents.weight",30000]}]
//                                            },
//                                              then:'$$this.groundhandling.W2030',
//                                            else:{
//                                              $cond:{
//                                                if:{
//                                                  $and:[{$gt:["$documents.weight",30000]},{$lte:["$documents.weight",40000]}]
//                                                },
//                                                  then:'$$this.groundhandling.W3040',
//                                                else:{
//                                                  $cond:{
//                                                    if:{
//                                                      $and:[{$gt:["$documents.weight",40000]},{$lte:["$documents.weight",50000]}]
//                                                    },
//                                                      then:'$$this.groundhandling.W4050',
//                                                    else:{
//                                                      $cond:{
//                                                        if:{
//                                                          $and:[{$gt:["$documents.weight",50000]},{$lte:["$documents.weight",70000]}]
//                                                        },
//                                                          then:'$$this.groundhandling.W5070',
//                                                        else:'$$this.groundhandling.W70M'
//                                                      }
//                                                    }
//                                                  }
//                                                }
//                                              }
//                                            }
//                                          }
//                                        }
//                                      }
//                                    }
//                                }
//                              }
//                            }
//                          }
//                      }
//                    }]}
//                  }
//                }
//              }
//            },{
//            $project:{
//            documents:"$documents",
//            calculatedDuration:"$calculatedDuration",
//            duration:"$duration",
//            price:"$price",
//            totalCrewCharge:"$totalCrewCharge",
//            groundHandlingCharges:"$groundHandlingCharges",
//            netFlyingCost:{$multiply:[{$divide:["$price",60]},"$calculatedDuration"]},
//            netcost:{
//              $let:{
//               vars:{
//               // totalBaseprice:{$multiply:["$price.price","$duration"]}
//               totalBaseprice:{$multiply:[{$divide:["$price",60]},"$calculatedDuration"]}
//               },
//               in:{$add:["$$totalBaseprice","$groundHandlingCharges","$totalCrewCharge"]}
//              }
//              }
//            }
//            },{
//            $project:{
//              netFlyingCost:"$netFlyingCost",
//              calculatedDuration:"$calculatedDuration",
//            	price:"$price",
//            gst:{$multiply:["$netcost",0.18]},
//             total:{$add:["$netcost",{$multiply:["$netcost",0.18]}]},
//             documents:"$documents",
//             airports:"$airports",
//             groundHandlingCharges:"$groundHandlingCharges",
//             netcost:"$netcost",
//             duration:"$duration",
//             totalCrewCharge:"$totalCrewCharge"
//            }
//            }]



/*
,{
    $project:{
        positioning:{
            $cond:{
                if:"$hasBase",
                then:[],
                else:{
                    
                }
            }
        }
    }
}
*/
