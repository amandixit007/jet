// admin routes//

var express = require('express'); 
var path=require('path');   
var router = express.Router();  
var authorisation = require('../authorisation');
var controllers = require('./controllers');


// router.post('/country',controller.addUpdateCounty)
// router.get('/country',controller.getAllCountries)
router.get('/bulkuploadAirport',controllers.bulkuploadAirport);
router.get('/bulkuploadAirportDistance',controllers.bulkuploadAirportDistance);
router.get('/bulkuploadHandlingCharges',controllers.bulkuploadHandlingCharges);
// router.get('/allairportDistance/:limit/:offset',controllers.allairportDistance);
router.get('/allairportDistance/:page_number',controllers.allairportDistance);

router.post('/addUpdateAirport',controllers.addUpdateAirport);
router.post('/addUpdateAirportDistance',controllers.addUpdateAirportDistance);
router.get('/addNewCitieStates',controllers.addNewCitieStates);

// router.get('/getAllaircraft',controllers.getAllaircraft)
// router.post('/aircraftDetail',controllers.aircraftDetail);
// router.post('/removeimage',controllers.removeimage);





module.exports = router;  