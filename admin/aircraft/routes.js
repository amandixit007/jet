// admin routes//

var express = require('express'); 
var path=require('path');   
var router = express.Router();  
var authorisation = require('../authorisation');
var controllers = require('./controllers');


// router.post('/country',controller.addUpdateCounty)
// router.get('/country',controller.getAllCountries)
router.post('/createUpdateAircraft',controllers.createUpdateaircraft)
router.get('/getAllaircraft',controllers.getAllaircraft)
router.post('/aircraftDetail',controllers.aircraftDetail);
router.post('/removeimage',controllers.removeimage);



router.post('/createUpdateFlight',controllers.createUpdateFlight)
router.get('/viewAlladdedFlight/:_id',controllers.viewAlladdedFlight)
router.post('/flightDetail',controllers.flightDetail)
router.post('/deleteFlight',controllers.deleteFlight)

router.get('/bulkuploadPrice',controllers.bulkuploadPrice)

router.get('/bulkuploadAircraft',controllers.bulkuploadAircraft)
router.get('/addAircraftWeight',controllers.addAircraftWeight)
router.get('/bulkUploadImg',controllers.bulkUploadImg)
router.get('/bulkuploadAddBase',controllers.bulkuploadAddBase)


router.post('/delete_image',controllers.delete_image)
router.get('/from_to_airport',controllers.from_to_airport)
router.post('/searchFlights',controllers.searchFlights)
router.post('/filterTest',controllers.filterTest)

module.exports = router;  