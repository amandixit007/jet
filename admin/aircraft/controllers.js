var config=require.main.require('./global/config');
var aircraft=require.main.require('./db/aircraftSchema.js');
var airport=require.main.require('./db/airportSchema.js');
var flight=require.main.require('./db/addFlightSchema.js');
var agent = require.main.require('./db/agentSchema');
var airportdistance = require.main.require('./db/airportDistanceSchema.js')
var countries = require.main.require('./db/countrySchema.js')
var price = require.main.require('./db/priceSchema.js');
var airportCtrl=require.main.require('./admin/airport/controllers.js');
var xmlCsvParser=require.main.require('./global/xmlCsvParser');
// var price = require.main.require('./db/priceSchema.js');
var mongoose = require('mongoose');
var https = require('https');
var async = require('async');
var waterfall = require('async-waterfall');
var ObjectId = require('mongoose').Types.ObjectId; 
var fs = require('fs');
var path=require('path')
var mv = require('mv');


var aircraft_api = {
  bulkUploadImg:function(req,res,next){
    // var dirpath=path.join(__dirname,"temp");
    var dirpath=path.join(process.env.PWD,"media","aircraftPics");
    console.log(dirpath);
    async.waterfall([function(cb){
      fs.readdir(dirpath, function(err, items) {
        console.log("err--->",err);
        console.log(items);
        async.map(items,function(x,cbMap){
          fs.readdir(path.join(dirpath,x), function(err, itemsFiles) {
            if(err)
              cbMap(err,null);
            else{
              console.log("itemsFiles--->",itemsFiles);
              console.log("x--->",x);
              /*
              db.collection.find( {"name_lower":
    { $regex: new RegExp("^" + thename.toLowerCase(), "i") } }
);
              */
              var aircraftName=x.split('~').join(" ");
              var files=itemsFiles.map(y=>{return path.join("media","aircraftPics",x,y)});
              
              aircraft.findOneAndUpdate({jet_name:{ $regex: new RegExp("^" + aircraftName, "i") }},{$addToSet:{image_gallary:{$each:files}}})
              // aircraft.findOne({jet_name:{ $regex: new RegExp("^" + x, "i") }})
              .exec(function(err,result){
                console.log("aircraft.findOneAndUpdate :: err---->",err)
                console.log("aircraft.findOneAndUpdate :: result---->",result)
                err?cbMap(err,null):cbMap(null,result);
              })
            }
          })
        },function(err,result){
          err?cb(err,null):cb(null,result)
        })
      });
    },function(files,cb){
      //async.ma
      //aircraft
    }],function(err,result){
      err?res.status(500).send(err):res.send(result);
    })
    
  },
  bulkuploadAircraft:function(req,res,next){
    //req.body.filesPath="temp/1506513772687.xlsx";
    var missingCities=[]
    async.waterfall([function(cb){
        xmlCsvParser.parse("temp/1510750359772.xlsx")
     // xmlCsvParser.parse("temp/1510726279394.xlsx")
        .then(function(objS){
            cb(null,objS["Sheet1"])
        },function(objE){
          cb(objE,null)
        })
    },function(xclData,cb){
      console.log("xcalDataaa--------",xclData)
      async.map(xclData,function(x,cbMap){
        var obj={
          jet_name:x["AIRCRAFT"],
          aircraft_type:x["Type"],
          speed :x["SPEED (Knots/hr.)"],
          weight :x["TAKE OFF WEIGHT (KGS.)"],
          'facilities.lavatory':x["LAVATORY"],
          // 'cabin_information.cabin_hight':x["Cabin Height"],
          passangers:x["Seating Capacity (Pax)"],
          pilot:x["Number of pilot"],
          aircraft_max_range:x["MAX RANGE (NM)"],
          baggage :x["TAKE OFF WEIGHT (KGS.)"],
          minimum_distance :x["MIN. DISTANCE REQUIRED IN FEET"],
          crew_members : x["crew Members"]

        }
        var aircraft_save = new aircraft(obj)
        aircraft_save.save(function(err,result){
          err?cbMap(err,null):cbMap(null,result)
        })
        // obj.save(function(err,result){
          
        // })
      },function(err,result){
        err?cb(err,null):cb(null,result);
      })
    }],function(err,result){
      if(err)
        console.log(err)
      console.log("missingCities---->",missingCities)
        err?res.status(500).send(err):res.send(missingCities);
    })
  },


bulkuploadAddBase:function(req,res,next){
//  async.waterfall([function(cb){
//   // xmlCsvParser.parse("temp/1508488777921.xlsx")
//    xmlCsvParser.parse("temp/1509534280230.xlsx")
//        .then(function(objS){
//            cb(null,objS["AIRCRAFT DATA"])
//        },function(objE){
//          cb(objE,null)
//        })
//      },function(data,cb){
//        console.log("data--->",data[0])
//        async.map(data,function(x,cbMap){
//        	var bases=[];
//        	if(x['BASE 1']!="****"){
//        		bases.push(x['BASE 1'])
//        	}
//        	if(x['BASE 2']!="****"){
//        		bases.push(x['BASE 2'])
//        	}
//        	if(x['BASE 3']!="****"){
//        		bases.push(x['BASE 3'])
//        	}
//        	if(x['BASE 4']!="****"){
//        		bases.push(x['BASE 4'])
//        	}
//        	if(x['BASE 5']!="****"){
//        		bases.push(x['BASE 5'])
//        	}
//        	if(x['BASE 6']!="****"){
//        		bases.push(x['BASE 6'])
//        	}
//
//        	var countryQuery=[
//        		{
//        			$match:{countryName:"India"}
//        		},
//        		{
//        			$unwind:"$states"
//        		},
//        		{
//        			$unwind:"$states.cities"
//        		},
//        		{
//        			$match:{"states.cities.cityName":{$in:bases}}
//        		},
//        		{
//        			$group:{_id:null,citiesArr:{$push:"$states.cities._id"}}
//        		}
//        	]
//        	countries.aggregate(countryQuery)
//        	.exec(function(err,resultCountry){
//        		if(resultCountry.length>0){
//        			// for(var i=0;i<resultCountry[0].citiesArr.length;i++){
//        			// 	console.log("mongoose.Types.ObjectId(x)--->",mongoose.Types.ObjectId(resultCountry[0].citiesArr[i]))
//        			// }
//
//        			var _bases=resultCountry[0].citiesArr.map(y=>{y=mongoose.Types.ObjectId(y); return y});
//	        		// console.log("resultCountry---->",resultCountry)
//	        		console.log("_bases---->",_bases)
//	        		// aircraft.find({jet_name:x["AIRCRAFT"]}).exec(function(error,result){
//	        		aircraft.update({jet_name:x["AIRCRAFT TYPE"]},{$set:{bases:_bases}},{upsert:false,mutli:true}).exec(function(error,result) {
//	        			error?cbMap(error,null):cbMap(null,result)
//	        		}) 
//        		}
//        		else{
//        			cbMap(null,resultCountry)
//        		}
//        		
//        	})
//        	/*
//				db.countries.aggregate({$match:{countryName:"India"}},{$unwind:"$states"},{$unwind:"$states.cities"},{$match:{"states.cities.cityName":{$in:["NEW DELHI","MUMBAI"]}}},{$project:{cityId:"$states.cities._id",cityName:"$states.cities.cityName"}})
//        	*/
//        	
//        },function(err,result){
//        	err?cb(err):cb(null,result)
//        })
//        //err?(cb,null):cb(null,data)
//      }],function(err,result){
//        err?res.status(500).send(err):res.send(result);
//  })
    
    
    
    async.waterfall([function(cb){
   // xmlCsvParser.parse("temp/1508488777921.xlsx")
    xmlCsvParser.parse("temp/1510750359772.xlsx")
        .then(function(objS){
            cb(null,objS["Sheet1"])
        },function(objE){
          cb(objE,null)
        })
      },function(data,cb){
        async.map(data,function(x,cbMap){
            console.log("xxx------",x)

             var bases=[{
                    base_city:"5a1569aa9bbcbe86e2a32d3c",
                    base_price:x["5a1569aa9bbcbe86e2a32d3c"]|| 0
                },{
                    base_city:"5a1569aa9bbcbe86e2a32d04",
                    base_price:x["5a1569aa9bbcbe86e2a32d04"]|| 0
                },{
                    base_city:"5a1569ab9bbcbe86e2a32e06",
                    base_price:x["5a1569ab9bbcbe86e2a32e06"]|| 0
                },{
                    base_city:"5a1569aa9bbcbe86e2a32cef",
                    base_price:x["5a1569aa9bbcbe86e2a32cef"]|| 0
                },{
                    base_city:"5a1569ab9bbcbe86e2a32e67",
                    base_price:x["5a1569ab9bbcbe86e2a32e67"]|| 0
                },{
                    base_city:"5a1569aa9bbcbe86e2a32d7a",
                    base_price:x["5a1569aa9bbcbe86e2a32d7a"]|| 0
                },{
                    base_city:"5a1569ab9bbcbe86e2a32df5",
                    base_price:x["5a1569ab9bbcbe86e2a32df5"]|| 0
                },{
                    base_city:"5a1569aa9bbcbe86e2a32d66",
                    base_price:x["5a1569aa9bbcbe86e2a32d66"]|| 0
                },
                {
                    base_city:"5a1569ab9bbcbe86e2a32dae",
                    base_price:x["5a1569ab9bbcbe86e2a32dae"]|| 0
                },{
                    base_city:"5a1569aa9bbcbe86e2a32d0d",
                    base_price:x["5a1569aa9bbcbe86e2a32d0d"]|| 0
                },{
                    base_city:"5a1569aa9bbcbe86e2a32d46",
                    base_price:x["5a1569aa9bbcbe86e2a32d46"]|| 0
                }]
          //    aircraft.update(query1,bases,{upsert:true,new:true}).exec(function(error,result) {
            aircraft.update({jet_name:x["AIRCRAFT"]},{$set:{bases:bases}}).exec(function(error,result) {
	        			error?cbMap(error,null):cbMap(null,result)
	        		})
             	
        },function(err,result){
        	err?cb(err):cb(null,result)
        })
        //err?(cb,null):cb(null,data)
      }],function(err,result){
        err?res.status(500).send(err):res.send(result);
  })
    

},

bulkuploadPrice:function(req,res,next){
    //req.body.filesPath="temp/1506513772687.xlsx";
    console.log("bulkuploadAirportDistance----->")
    var missingCities=[]
    async.waterfall([function(cb){
     //   xmlCsvParser.parse("temp/1507094282066.xlsx")
        xmlCsvParser.parse("temp/1509534280230.xlsx")
        .then(function(objS){
            cb(null,objS["AIRCRAFT DATA"])
        },function(objE){
          cb(objE,null)
        })
    },function(xclData,cb){
      //console.log("xcalDataaa--------",xclData)
      async.map(xclData.filter(x=>!isNaN(x["PRICE PER HOUR"])),function(x,cbMap){

            aircraft.find({jet_name:x["AIRCRAFT TYPE"]},function(err,result1){
            	//console.log("result1",result1);
                  if(err){
                	cbMap(err,null)
                }
                   else{
		               	if(result1.length>0){
		                	x.aircraft_id=result1[0];
		                	//console.log("x--->",x)
				    		var obj={
				    			aircraft_id:x.aircraft_id._id,
				    			price:x["PRICE PER HOUR"],
				    		//	REG:x["REG"],
				    			createdBy:"admin"
				    		}
				    		price.findOneAndUpdate({aircraft_id:x.aircraft_id._id},obj,{new:true,upsert:true},function(err,result){
				  				console.log("err-->",err);
				    			console.log("updatd",result)
						    			err?cbMap(err,null):cbMap(null,x);
				    		});
		               	}
		               	else{
		               		cbMap(null,x);
		               	}
                	
		    	}
		    
            })
      },function(err,result){
      	err?cb(err,null):cb(null,result);
      })
    }],function(err,result){
      if(err)
        console.log(err)
      console.log("result---->",result)
        err?res.status(500).send(err):res.send(result);
    })
  },



















removeimage:function(req,res){
    
},
createUpdateaircraft : function(req,res){
    console.log("request---",req.body.image_gallary)
    var _rootPath=process.env.PWD;
    console.log("_rootPath_rootPath",_rootPath) 
    _dirName=req.body.jet_name.split(' ').join('~');
   var _dirPath=path.join(_rootPath,"media","aircraftPics",_dirName)
    async.waterfall([function(cb){

        // for(var i=0;i<req.body.image_gallary.length;i++){
        //     var _filePath=path.join(_rootPath,req.body.image_gallary[i])
        //     console.log("filepathfile",_filePath);
        //     // fs.readFile('/etc/passwd', function (err, data) {   
        //     //     if (err) throw err;   console.log(data); 
        //     //   });
        //  }
         
         console.log("_folderName_folderName",_dirName);
         var _isDirExist=fs.existsSync(_dirPath);
         console.log("dirpath",_dirPath);
            // var _name=JSON.stringify(new Date().getTime())+"."+req.files.temp.name.split('.')[1];
            if(!_isDirExist){
                fs.mkdir(_dirPath, function(err,result){
                  if(err){
                      console.log("errerrr",err);
                  }
                  else{
                      fs.chmodSync(_dirPath,0777)
                      console.log("result",result);
                       cb(null,_dirPath)
                  }
                })
            }
            else{
               
                cb(null,_dirPath)
                 
            }
        
    },function(filepath,cb){
      async.map(req.body.image_gallary,function(x,cbMap){
         var fileName=x.split('/')[1];
         var _sourcePath=path.join(_rootPath,x)
         var _destinationPath=path.join(_dirPath,fileName)
         console.log("_sourcePath",_sourcePath);
         console.log("destinationpath",_destinationPath);
          
         mv(_sourcePath,_destinationPath, function(err) {
          console.log("errerr",err);
              err?cbMap(err,null):cbMap(null,path.join("media","aircraftPics",_dirName,fileName)); 
           });
      },function(err,result){
        console.log("destinationpath",result);
        err?cb(err,null):cb(null,result)
      })
        
    },function(saveFiledata,cb){
      req.body.image_gallary=saveFiledata;
      console.log("req.body---->",req.body)
        if(!req.body._id){
            var aircraft_save=new aircraft(req.body)
            aircraft_save.save(function(error,result){
              if(error){
                console.log(error);
                cb(error)
              }
              else{
               console.log("result",result);
               cb(null,result) 
             }
                  
              },function(err){
                  cb(err,null)
            })
        }
        else{
            aircraft.findByIdAndUpdate(req.body._id,{$set:{image_gallary:saveFiledata}}, function(error,result) {
            if(error) { return res.send({status:500,message:"something went wrong"}) }
            //error?res.status(500).send(error):res.send(result)
                console.log("result",result);
                   cb(null,result)
                  
                  },function(err){
                      cb(err,null)
            })
        }
      
   }],function(err,result){
        if(err)
            //api_response_handler.api_response_without_data(req,res,500,'Server error')
           res.status(500).send(err)
        else
             //  api_response_handler.api_response_with_data(req,res,200,'Successfully',result)
           res.send(result);
    })

    
},

'getAllaircraft':function(req,res){
   aircraft.find({},function(err,result){
    err?res.status(500).send(err):res.send(result)

   })

},

'aircraftDetail': function(req,res){
    console.log("req---",req.body);
    aircraft.findOne({_id:req.body.aircraftId},function(err,result){
         if(error) { return res.send({status:500,message:"something went wrong"})}
            else if(!result){return res.send({status:400,message:"No data found"})}
                else{return res.send({status:200,message:"Aircraft Found"})}
        // error?res.status(500).send(error):res.send(result)     

    })
},

'createUpdateFlight' : function(req,res){
  console.log("requets"+JSON.stringify(req.body));
  if(!req.body._id){
    var flight_save = new flight(req.body);
    flight_save.save(function(err,result){

   err?res.status(500).send(err):res.send(result)
            })
  }
  else{
    flight.findByIdAndUpdate(req.body._id, {$set:req.body}, function(error,result) {
            if(error) { return res.send({status:500,message:"something went wrong"}) }
            error?res.status(500).send(error):res.send(result)

  })

}

},

 'viewAlladdedFlight' : function(req,res){
    flight.find({created_by:req.params._id,status:"active"},function(err,result){
    err?res.status(500).send(err):res.send(result)
    })
 },

 'flightDetail': function(req,res){
  flight.findOne({_id:req.body.flight_id},function(err,result){
  	  // flight.findOne({origin:new RegExp(req.body.origin, 'i')},function(err,result){
 err?res.status(500).send(err):res.send(result)

  })

 },
addAircraftWeight:function(req,res,next){
	async.waterfall([function(cb){
        xmlCsvParser.parse("temp/1507039826556.xlsx")
        .then(function(objS){
            cb(null,objS["Aircrafts Detail"])
        },function(objE){
          cb(objE,null)
        })
    },function(xclData,cb){
      console.log("xcalDataaa--------",xclData)
      async.map(xclData,function(x,cbMap){
        aircraft.update({
        	jet_name:x["AIRCRAFT TYPE"]
        },{
        	$set:{
        		weight:x["TAKE OFF WEIGHT (KGS.)"]
        	}
        })
        .exec(function(err,result){
        	err?cbMap(err,null):cbMap(null,result)
        })
        
      },function(err,result){
        err?cb(err,null):cb(null,result);
      })
    }],function(err,result){
      if(err)
        console.log(err)
        err?res.status(500).send(err):res.send(result);
    })
},
 'deleteFlight' : function(req,res){
   flight.findByIdAndUpdate({_id:req.body.flight_id},{$set:{status:"delete"}},function(err,result){
 err?res.status(500).send(err):res.send(result)

   })

 },


// var obj={};
// 		for(var key in data)
// 			obj[key]=data[key];


// 59d374b48a4428d5b7de9a92
// 59d374b48a4428d5b7de9a7b



	 'searchFlights':function(req,res){
	  console.log("request",req.body);
	  _planeStandByTime=20;
var obj = req.body.routs
var _roots=[];
 var dateDef  = 0,minimumDuration=0;
          var minRunway = 0;
 var extraHalt=0;
      	if(req.body.tripType=="oneWay"){
      		req.body.roots[0].flightDuration=0;
          _roots.push(req.body.roots[0]);
          var objRoots={
            departureDate:"",
            departureTime:"",
            flightDuration:0,
            destination:req.body.roots[0].origin,
            destinationName:req.body.roots[0].originName,
            origin:req.body.roots[0].destination,
            originName:req.body.roots[0].destinationName
          };
      		req.body.roots.push(objRoots);
          _roots.push(objRoots);
      	}
      	else{
          console.log("req.body.roots[req.body.roots.length-1].departureDate-------",req.body.roots[req.body.roots.length-1].departureDate);
          console.log("req.body.roots[0].departureDate--------",req.body.roots[0].departureDate)

           dateDef = new Date(req.body.roots[req.body.roots.length-1].departureDate).getDate() - new Date(req.body.roots[0].departureDate).getDate()
          // dateDef = Math.ceil(dateDef/(1000*60*60*24))
           console.log("dateDef--->",dateDef)
          // new Date(req.body.roots[i+1].departureDate).getDate()- new Date(req.body.roots[i].departureDate).getDate();
           minimumDuration = (dateDef+1)*120
           console.log("minimumDuration--->",minimumDuration);
      		for(var i=0;i<req.body.roots.length;i++){
      			req.body.roots[i].flightDuration=0;
      			if(i<(req.body.roots.length-1)){
              var dayDeff=new Date(req.body.roots[i+1].departureDate).getDate()- new Date(req.body.roots[i].departureDate).getDate();
              if(dayDeff)
              extraHalt+=dayDeff-1;
    					req.body.roots[i].flightDuration=new Date(req.body.roots[i+1].departureDate+" "+req.body.roots[i+1].departureTime)-new Date(req.body.roots[i].departureDate+" "+req.body.roots[i].departureTime);
              req.body.roots[i].flightDuration=req.body.roots[i].flightDuration/1000/60
    				}
            else{
              req.body.roots[i].flightDuration=0;
            }
    				
				
      		}
          _roots=req.body.roots;
          console.log("_roots--->",_roots)
      	}
		
		//new Date(tt.roots[0].departureDate+" "+tt.roots[0].departureTime)
	  
    	if(req.body.searchType == "normalSearch"){
         waterfall([
           function(cb){
    
            async.parallel({
              roots: function(callback) {
              
                  // async.map(req.body.roots.filter(x=>x.fromAirport.length>0&& x.toAirport.length>0),airportCtrl.airportdistance.bind(airportCtrl),function(err,result){

                  	 async.map(_roots,airportCtrl.airportdistance.bind(airportCtrl),function(err,result){
                  	
                  	console.log("result",result)
                  	for(var i=0 ;i<result.length;i++){
                  		if(result[i].distance == 0){
                  			console.log("enter in if")
                  			return res.send({message:"No route found"})
                  		}
                  		// err?callback(err,null):callback(null,result);
                  	}
                    err?callback(err,null):callback(null,result);
                  })
              },
              groundhandling: function(callback) {
                var airportsInJourney=[];
                  console.log("req.body.roots---->",_roots);

                  for(var i=0;i<_roots.length;i++){
                    if(airportsInJourney.indexOf(_roots[i].origin)==-1)
                      airportsInJourney.push(_roots[i].origin)
                    if(airportsInJourney.indexOf(_roots[i].destination)==-1)
                      airportsInJourney.push(_roots[i].destination)
                  }
                   async.map(airportsInJourney,airportCtrl.groundhandlingCharge.bind(airportCtrl),function(err,result){
                      err?callback(err,null):callback(null,result);
                   })

                },
                baseDistance:function(callback){
                    var _bases=config.airportBases.map(x=>{
                        return {
                            fromAirport:mongoose.Types.ObjectId(x.airportId),
                            toAirport:mongoose.Types.ObjectId(_roots[0].origin)
                        };
                    })
                    var query={
                        $or:_bases
                    }
                    async.map(_bases,function(x,cbMap){
                              airportdistance.find(x).exec(function(err,result){
                       if(err){
                           cbMap(err)
                       }
                      else{
                          if(result.length == 0){
                              cbMap(null,{
                                 fromAirport:ObjectId(x.fromAirport),
                                  toAirport:ObjectId(x.toAirport),
                                  distance:2000000
                              })
                          }
                          else{
                              console.log("result[0]---",result)
                              cbMap(null,{
                                 _id:result[0]._id,
                                 fromAirport:ObjectId(result[0].fromAirport),
                                  toAirport:ObjectId(result[0].toAirport),
                                  distance:result[0].distance
                              })
                          }
                      }
                    })
                        
                    },function(err,result){
                          err?callback(err,null):callback(null,result);
                    })
          
                },
                reverseBaseDistance:function(callback){
                    var _bases=config.airportBases.map(x=>{
                        return {
                            fromAirport:mongoose.Types.ObjectId(_roots[0].origin),
                            toAirport:mongoose.Types.ObjectId(x.airportId)
                        };
                    })
          
                    var query={
                        $or:_bases
                    }
                    async.map(_bases,function(x,cbMap){
                      airportdistance.find(x).exec(function(err,result){
                       if(err){
                           cbMap(err)
                       }
                      else{
                          if(result.length == 0){
                              cbMap(null,{
                                 fromAirport:mongoose.Types.ObjectId(x.fromAirport),
                                  toAirport:mongoose.Types.ObjectId(x.toAirport),
                                  distance:2000000
                              })
                          }
                          else{
                              cbMap(null,{
                                    _id:result[0]._id,
                                 fromAirport:ObjectId(result[0].fromAirport),
                                  toAirport:ObjectId(result[0].toAirport),
                                  distance:result[0].distance
                              })
                          }
                      }
                    })
                        
                    },function(err,result){
                        console.log("repo result------------",result)
                          err?callback(err,null):callback(null,result);
                    })
          
                }
              }, 
                           function(err, results) {
                err?(err,null,null):cb(null,results.roots,results.groundhandling,results.baseDistance,results.reverseBaseDistance);

              });
           
            },
           function(roots,airports,baseDistance,reverseBaseDistance,cb){
               for(var i=0;i<baseDistance.length;i++){ 
                   console.log("check",i,baseDistance[i])
                   baseDistance[i]['indx'] = i;
        
               }
                  for(var i=0;i<reverseBaseDistance.length;i++){ 
                   console.log("check",i,baseDistance[i])
                   reverseBaseDistance[i]['indx'] = i;
        
               }
               
              
          //     baseDistance=baseDistance.map((x,indx)=>{ x['indx'] = i; return x});
//               reverseBaseDistance=reverseBaseDistance.map((x,indx)=>{{x.indx=indx; return x}});


              var minRunway = Math.min.apply(Math,airports.map(function(o){return o.runway;}))

            var query=[{
                    $match:{
            minimum_distance:{$lte:minRunway}
        
    }
//    $match:{
//        $and:[{
//            $or:[{minimum_distance:{$gt:5000}},{minimum_distance:{$lte:3000}}]
//        },{
//            aircraft_max_range:{$gte:1500}
//        }]
//    }
},{
    $addFields:{
        "hasBase":{
            $gt:[{
                $size:{
                $filter:{
                    input:"$bases",
                    as:"x",
                    cond:{
                        $and:[{
                           $eq:["$$x.base_city",mongoose.Types.ObjectId(_roots[0].origin)]
                          //    $eq:["$$x.base_city",0]
                  
                        },{
                            $gt:["$$x.base_price",0]
                        }]
                    }
                }
            }
            },0]
            
        },
        activeBases:{
           $filter:{
                input:"$bases",
                as:"x",
                cond:{
                   $gt:["$$x.base_price",0]
                }
            }
        },
        baseDistance:baseDistance,
        reverseBaseDistance:reverseBaseDistance
    }
},{
    $addFields:{
        positioning:{
            $reduce:{
                input:{
                    $map:{
                        input:"$activeBases",
                        as:"x",
                        in:{
                            price:"$$x.base_price",
                            obj:{
                                $arrayElemAt:["$baseDistance",{
                                $reduce:{
                                    input:"$baseDistance",
                                    initialValue:-1,
                                    in:{
                                        $cond:{
                                            if:{$eq:["$$x.base_city","$$this.fromAirport"]},
                                            then:"$$this.indx",
                                            else:"$$value"
                                        }
                                    }
                                }
                            }]
                            }
                            
                        }
                    }
                },
                initialValue:{fromAirport:"",toAirport:"",distance:10000000000,price:0},
                in:{
                    $cond:{
                        if:{$eq:["$$value.distance",10000000000]},
                        then:{fromAirport:"$$this.obj.fromAirport",toAirport:"$$this.obj.toAirport",distance:"$$this.obj.distance",price:"$$this.price"},
                        else:{
                            $cond:{
                                if:{
                                    $lt:["$$value.distance","$$this.distance"]
                                },
                                then:{fromAirport:"$$value.fromAirport",toAirport:"$$value.toAirport",distance:"$$value.distance",price:"$$value.price"},
                                else:{fromAirport:"$$this.obj.fromAirport",toAirport:"$$this.obj.toAirport",distance:"$$this.obj.distance",price:"$$this.obj.price"}
                            }
                        }
                    }
                }
            }
        }
    }
},{
 $addFields:{
         repositioning:{
            $arrayElemAt:[
                {
                    $filter:{
                        input:"$reverseBaseDistance",
                        as:"x",
                        cond:{
                            $and:[{$eq:["$$x.toAirport","$positioning.fromAirport"]},{$eq:["$$x.fromAirport","$positioning.toAirport"]}]
                        }
                    }
                },0]
        }
    }
},{
  $addFields:{
      positioning:{
            "fromAirport" : "$positioning.fromAirport",
            "toAirport" : "$positioning.toAirport",
            "distance" : "$positioning.distance",
            "price" : "$positioning.price",
            "isAvail":{
           $cond:{
                    if: { $eq: [ "$positioning.distance", 2000000 ] },
                            then:false,
                        else:true
                    }
                 },
           duration:{
                $add:[_planeStandByTime,{$multiply:[{$divide:["$positioning.distance","$$ROOT.speed"]},60]}]
            },
           cost:{
                $divide:[{
                    $multiply:["$positioning.price",{$add:[_planeStandByTime,{$multiply:[{$divide:["$positioning.distance","$$ROOT.speed"]},60]}]}]
                },60]
            }
        },
     repositioning:{
            fromAirport:"$repositioning.fromAirport",
            toAirport:"$repositioning.toAirport",
            distance:"$repositioning.distance",
            price:"$positioning.price",
            "isAvail":{
           $cond:{
                    if: { $eq: [ "$repositioning.distance", 2000000 ] },
                            then:false,
                        else:true
                    }
                 },
          duration:{
                $add:[_planeStandByTime,{$multiply:[{$divide:["$repositioning.distance","$$ROOT.speed"]},60]}]
            },
           cost:{
             $divide:[{
                    $multiply:["$positioning.price",{$add:[_planeStandByTime,{$multiply:[{$divide:["$repositioning.distance","$$ROOT.speed"]},60]}]}]
                },60]
            }
        }
    }
},{
 $project: {
        documents:"$$ROOT",
        airports:airports,
        price:"$positioning.price",
      duration: {
         $map:{
              input: roots,
              as: "x",
              in: { 
                "data":"$$x",
                "duration":{
                  $add:[_planeStandByTime,{$multiply:[{$divide:["$$x.distance","$$ROOT.speed"]},60]}]
                },
                "crewCharges":{
                    $cond:{
                        if:{$gt:[{$subtract:["$$x.flightDuration",{$add:[_planeStandByTime,{$multiply:[{$divide:["$$x.distance","$$ROOT.speed"]},60]}]}]},4*60]},
                            then:{$multiply:["$$ROOT.crew_charges",{$sum:["$$ROOT.pilot","$$ROOT.crew_members"]}]},
                        else:0
                    }
                }
              }
            }
          }
        }
},{
        $project:{
                documents:"$documents",
                price:"$price",
                duration:"$duration",
                calculatedDuration:{
                  $cond:{
                    if:{$gt:[minimumDuration,{
                      $reduce:{
                        input:"$duration",
                        initialValue:0,
                        in:{
                          $add:["$$value","$$this.duration"]
                        }
                      }
                    }]},
                    then:minimumDuration,
                    else:{
                      $reduce:{
                        input:"$duration",
                        initialValue:0,
                        in:{
                          $add:["$$value","$$this.duration"]
                        }
                      }
                    }
                  }
                }, totalCrewCharge:{
                  $sum:[{
                    $reduce:{
                      input:"$duration",
                      initialValue:0,
                      in:{
                        $add:["$$value","$$this.crewCharges"]
                      }
                    }
                  },{$multiply:[15000,extraHalt,{$sum:["$documents.pilot","$documents.crew_members"]}]}]
                },
                groundHandlingCharges:{
                  $reduce:{
                    input:"$airports",
                    initialValue:0,
                    in:{$add:["$$value", {
                      $cond:{
                        

                          if:{
                            $and:[{$gte:["$documents.weight",0]},{$lte:["$documents.weight",5000]}]
                          },
                            then:'$$this.groundhandling.W05',
                          else:{
                            $cond:{
                              if:{
                                $and:[{$gt:["$documents.weight",5000]},{$lte:["$documents.weight",10000]}]
                              },
                                then:'$$this.groundhandling.W510',
                              else:{
                                $cond:{

                                  if:{
                                    $and:[{$gt:["$documents.weight",10000]},{$lte:["$documents.weight",15000]}]
                                  },
                                    then:'$$this.groundhandling.W1015',
                                    else:{
                                      $cond:{
                                        if:{
                                          $and:[{$gt:["$documents.weight",15000]},{$lte:["$documents.weight",20000]}]
                                        },
                                        then:'$$this.groundhandling.W1520',
                                        else:{
                                          $cond:{
                                            if:{
                                              $and:[{$gt:["$documents.weight",20000]},{$lte:["$documents.weight",30000]}]
                                            },
                                              then:'$$this.groundhandling.W2030',
                                            else:{
                                              $cond:{
                                                if:{
                                                  $and:[{$gt:["$documents.weight",30000]},{$lte:["$documents.weight",40000]}]
                                                },
                                                  then:'$$this.groundhandling.W3040',
                                                else:{
                                                  $cond:{
                                                    if:{
                                                      $and:[{$gt:["$documents.weight",40000]},{$lte:["$documents.weight",50000]}]
                                                    },
                                                      then:'$$this.groundhandling.W4050',
                                                    else:{
                                                      $cond:{
                                                        if:{
                                                          $and:[{$gt:["$documents.weight",50000]},{$lte:["$documents.weight",70000]}]
                                                        },
                                                          then:'$$this.groundhandling.W5070',
                                                        else:'$$this.groundhandling.W70M'
                                                      }
                                                    }
                                                  }
                                                }
                                              }
                                            }
                                          }
                                        }
                                      }
                                    }
                                }
                              }
                            }
                          }
                      }
                    }]}
                  }
                }
              }
            },{
            $project:{
            documents:"$documents",
            calculatedDuration:"$calculatedDuration",
            duration:"$duration",
            price:"$price",
            totalCrewCharge:"$totalCrewCharge",
            groundHandlingCharges:"$groundHandlingCharges",
            netFlyingCost:{$multiply:[{$divide:["$price",60]},"$calculatedDuration"]},
            netcost:{
              $let:{
               vars:{
               // totalBaseprice:{$multiply:["$price.price","$duration"]}
               totalBaseprice:{$multiply:[{$divide:["$price",60]},"$calculatedDuration"]}
               },
             //  in:{$add:["$$totalBaseprice","$groundHandlingCharges","$totalCrewCharge"]}
       //           in:{$add:["$$totalBaseprice","$groundHandlingCharges","$totalCrewCharge","$documents.positioning.cost","$documents.repositioning.cost"]}
                   in:{$add:["$$totalBaseprice","$groundHandlingCharges","$totalCrewCharge",{
                       $cond:{
                            if:"$documents.positioning.isAvail",
                           then:"$documents.positioning.cost",
                           else:0
                       }
                   },{
                       $cond:{
                            if:"$documents.repositioning.isAvail",
                           then:"$documents.repositioning.cost",
                           else:0
                       }
                   }]}
              }
              }
            }
            },{
            $project:{
              netFlyingCost:"$netFlyingCost",
              calculatedDuration:"$calculatedDuration",
            	price:"$price",
            gst:{$multiply:["$netcost",0.18]},
             total:{$add:["$netcost",{$multiply:["$netcost",0.18]}]},
             documents:"$documents",
             airports:"$airports",
             groundHandlingCharges:"$groundHandlingCharges",
             netcost:"$netcost",
             duration:"$duration",
             totalCrewCharge:"$totalCrewCharge"
            }
            }]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
               
           aircraft.aggregate(query,function(err,result1){
              err?cb(err,null):cb(null,result1);
    	      // console.log("result1",result1);
    	     // for(var i=0;i< result1.length;i++){
    	     //  result1[i].price = result1[i].price*req.body.travellers
    	     //  result1[i].time = distance/result1[i].speed
    	     // }
    	     // cb(null, result1)
    		    })
         }

	      	],function(err,success){

	      		 err?res.status(500).send(err):res.send(success)

	      	})
	    	} 

	    	else {

	        waterfall([
	            
             function(cb){
             	airportdistance.findOne({fromAirport:req.body.origin,toAirport:req.body.destination},function(err,result){
           		var distance = (result.distance*config.static.nauticalDistance) // change nautical mile to km
           		console.log("ditance",distance)
                cb(null,distance)
            })

             },
             function(distance,cb){
               
       var condition = {
         	$or :[]
              };
         condition.$or.push({'facilities.wifi':req.body.wifi},{'facilities.flight_attendence':req.body.flight_attendence},{'facilities.lavatory':req.body.lavatory},{'aircraft_type':req.body.aircraftType})
           console.log("condition",condition)
        if (condition.$or.length == 0) {
            delete condition.$and;
        }

         var query=[{$lookup:{
              as:"price",
              from:'aircraftprices',
              localField:'_id',
              foreignField:'aircraft_id'
            }
            },
            {$project: {
            documents:"$$ROOT",
            price:{ $arrayElemAt: [ "$$ROOT.price", 0 ] },
              duration: {
                  $let: {
                      vars: {
                          dist:{$multiply: [1.852,distance]}
                      },
                      in: { $divide: ["$$dist","$speed"]}
                  }
              }
            }},{
            $project:{
            documents:"$documents",
            netcost:{
              $let:{
               vars:{
               totalBaseprice:{$multiply:["$duration","$price.price"]},
               groundHandlingCharge:2000
               },
               in:{$add:["$$totalBaseprice","$$groundHandlingCharge"]}
              }
              }
            }
            },{
            $project:{
            gst:{$multiply:["$netcost",0.18]},
             total:{$add:["$netcost",{$multiply:["$netcost",0.18]}]},
             documents:"$documents",
             netcost:"$netcost"
            }
            },{
            $sort:{duration:-1}
          }];

           var b = [];
              aircraft.aggregate(query,function(err,result1){
              err?cb(err,null):cb(null,result1);
            // console.log("result1",result1);
           // for(var i=0;i< result1.length;i++){
           //  result1[i].price = result1[i].price*req.body.travellers
           //  result1[i].time = distance/result1[i].speed
           // }
           // cb(null, result1)
            })
 
        // aircraft.find(condition,function(err,result1){
        // 	console.log("resut111",result1)
        
        // 	for(var i=0;i< result1.length;i++){
			     //  result1[i].price = result1[i].price*req.body.travellers
			     //    result1[i].time = distance/result1[i].speed
			     // }
        	
        //          cb(null, result1)
               
        // })

             }

        	],function(err,success){
                err?res.status(500).send(err):res.send(success)

        	})

    	}         
 },
    
   from_to_airport:function(req,res){
        console.log("enter");
      countries.aggregate([{$unwind:"$states"},{$unwind:"$states.cities"},{$lookup:{as:"airportList",from:"airports","localField":"states.cities._id",foreignField:"city"}}],function(err,result){
       err?res.status(500).send(err):res.send(result)

      })
    },

    delete_image:function(req,res){
   console.log("req",req.body)
   aircraft.update({_id:req.body.aircraft_id},{$pull:{image_gallary:{$in:[req.body.image_url]}}},function(err,result){
      err?res.status(500).send(err):res.send(result)

   })

    },


    


  
     'filterTest' : function(req,res){
     	console.log("request",req.body)
          var condition = {
                    $and: []
                };

      //  if(req.body['city']){
                    condition.$and.push({
                    city:req.body.city  
              
                })
       //         }
       console.log("condition",condition)
        if (condition.$and.length == 0) {
            delete condition.$and;
        }

        aircraft.find(condition,function(err,result){
             err?res.status(500).send(err):res.send(result)

        })
                

 }


}


module.exports = aircraft_api;



    // waterfall[(
      //            function(cb){
                 




      //            },
      //            function(result,cb){


      //            }

      //  ],function(err,success){


      //  })